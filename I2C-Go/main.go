package main

import (
	"fmt"
	"log"
	"math"
	"time"

	"github.com/d2r2/go-i2c"
	"github.com/d2r2/go-logger"
)

const (
	DEVICE_BUS     = 1
	DEVICE_ADDRESS = 0x10
	RELAY1         = 0x1
	ON             = 0xFF
	OFF            = 0x00

	RELAY_PCF8574 = 7 // Pin 7 (van del 0 al 7)
)

var conn *i2c.I2C

func main() {
	var err error
	logger.ChangePackageLogLevel("i2c", logger.ErrorLevel)
	conn, err = i2c.NewI2C(DEVICE_ADDRESS, DEVICE_BUS)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	// Write
	err = Write()
	if err != nil {
		log.Fatal(err)
	}

	// Write PCF8574
	err = WritePCF8574()
	if err != nil {
		log.Fatal(err)
	}
}

func Write() error {
	// Send pulse 0.2s
	_, err := conn.WriteBytes([]byte{RELAY1, ON})
	if err != nil {
		return err
	}

	time.Sleep(200 * time.Millisecond)

	_, err = conn.WriteBytes([]byte{RELAY1, OFF})
	if err != nil {
		return err
	}

	time.Sleep(200 * time.Millisecond)

	return nil
}

func WritePCF8574() error {
	// Send pulse 1s
	_, err := conn.WriteBytes([]byte{1 << 7})
	if err != nil {
		return err
	}

	time.Sleep(1 * time.Second)

	_, err = conn.WriteBytes([]byte{0b00000000})
	if err != nil {
		return err
	}

	time.Sleep(1 * time.Second)

	return nil
}

func ReadPCF8574() error {
	conn, err := i2c.NewI2C(DEVICE_ADDRESS, DEVICE_BUS)
	if err != nil {
		return err
	}

	currentState := 0.0
	for {
		buf := make([]byte, 1)
		_, err := conn.ReadBytes(buf)
		if err != nil {
			continue
		}

		state := math.Log2(float64(buf[0]))

		if state != 0 && currentState == 0 { // Se enciende
			fmt.Println(fmt.Sprint(RELAY1) + "is ON")
			currentState = state
		} else if state == 0 && currentState != 0 { // Se apaga
			fmt.Println(fmt.Sprint(RELAY1) + "is OFF")
			currentState = state
		}
		time.Sleep(20 * time.Millisecond)
	}
}
