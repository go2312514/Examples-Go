package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

const (
	BROKER_HOST = "localhost"
	BROKER_PORT = "1883"
	CLIENT_ID   = "testclient"
	TOPIC_NAME  = "topic/name"
)

type MqttMessage struct {
	Message string `json:"message"`
}

var f mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("RECEIVED NEW MESSAGE > TOPIC: %s\tMSG: %s\n", msg.Topic(), msg.Payload())
	decodedMsg := MqttMessage{}
	err := json.Unmarshal(msg.Payload(), &decodedMsg)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Decoded msg: %s\n", decodedMsg.Message)
}

func main() {
	// Setup
	// mqtt.DEBUG = log.New(os.Stdout, "", 0)
	mqtt.ERROR = log.New(os.Stdout, "", 0)
	opts := mqtt.NewClientOptions().AddBroker("tcp://" + BROKER_HOST + ":" + BROKER_PORT).SetClientID(CLIENT_ID)
	opts.SetKeepAlive(2 * time.Second)
	opts.SetDefaultPublishHandler(f)
	opts.SetPingTimeout(1 * time.Second)

	c := mqtt.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		log.Fatal(token.Error())
	}

	// Subscribe
	if token := c.Subscribe(TOPIC_NAME, 0, nil); token.Wait() && token.Error() != nil {
		log.Fatal(token.Error())
	}

	// Publish
	for i := 0; i < 5; i++ {
		fmt.Println("Publishing new message...")

		msg, err := json.Marshal(MqttMessage{Message: "this is message" + fmt.Sprint(i)})
		if err != nil {
			log.Fatal(err)
		}
		token := c.Publish(TOPIC_NAME, 0, false, msg)
		// token.Wait() // This will wait until message is received
		_ = token

		time.Sleep(1 * time.Second)
	}

	time.Sleep(6 * time.Second)

	// Unsubscribe and disconnect
	if token := c.Unsubscribe(TOPIC_NAME); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}

	c.Disconnect(250)

	time.Sleep(1 * time.Second)
}
