package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sync"

	"github.com/t3rm1n4l/go-mega"
)

var m *mega.Mega

func main() {
	m = mega.New()

	// Login
	err := m.Login("mega_user", "mega_password")
	if err != nil {
		log.Fatal(err)
	}
	// Deep tree print
	// treePrint(0, m.FS.GetRoot())

	// Get node by name
	// n1 := deepNodeSearch("Scan2023-07-04_113658.pdf", m.FS.GetRoot())
	// fmt.Println("ARCHIVO:", n1.GetName())

	// Get node by hash (it is in the mega url)
	// n2 := m.FS.HashLookup("Vn9TEIra")
	// fmt.Println("FOLDER:", n2.GetName())

	// Download
	// n2 := m.FS.HashLookup("B7s3mLyY")
	// fmt.Println("FOLDER:", n2.GetName())
	// DownloadTree(n2, "downloads")

	// Count files
	// n2 := m.FS.HashLookup("B7s3mLyY")
	// fmt.Println("FOLDER:", n2.GetName())
	// fmt.Println(countFiles(n2))

	// fmt.Println(countFilesLocal("downloads"))
}

func deepTreePrint(level int, n *mega.Node) {
	tab := ""
	for i := 0; i < level; i++ {
		tab += "\t"
	}
	fmt.Println(tab + n.GetName())
	// Base case
	if n.GetType() == 0 {
		return
	}
	// Recursive case
	children, _ := m.FS.GetChildren(n)
	for _, child := range children {
		deepTreePrint(level+1, child)
	}
}
func deepNodeSearch(name string, n *mega.Node) *mega.Node {
	// Base case
	if n.GetName() == name {
		return n
	} else if n.GetType() == 0 {
		return nil
	}
	// Recursive case
	children, _ := m.FS.GetChildren(n)
	for _, child := range children {
		node := deepNodeSearch(name, child)
		if node != nil {
			return node
		}
	}
	return nil
}

func DownloadTree(n *mega.Node, destPath string) {
	os.Mkdir("downloads", 0755)
	wg := sync.WaitGroup{}
	deepDownload(n, "downloads", &wg)
	wg.Wait()
}
func deepDownload(n *mega.Node, destPath string, wg *sync.WaitGroup) {
	// Base case: Download file to destination path
	if n.GetType() == 0 {
		wg.Add(1)
		go func() {
			err := m.DownloadFile(n, destPath+string(os.PathSeparator)+n.GetName(), nil)
			if err != nil {
				fmt.Println("\033[31m" + fmt.Sprint(err) + " " + destPath + string(os.PathSeparator) + n.GetName() + "\033[0m")
			}
			fmt.Println(n.GetName())
			wg.Done()
		}()
		return
	}
	// Recursive case: Create folder and dive inside folder
	if n.GetType() == 1 {
		// Create the folder if not exists
		destPath += string(os.PathSeparator) + n.GetName()
		os.Mkdir(destPath, 0755)
	}
	children, _ := m.FS.GetChildren(n)
	for _, child := range children {
		deepDownload(child, destPath, wg)
	}
}

func countFiles(n *mega.Node) int {
	// Base case
	if n.GetType() == 0 {
		return 1
	}
	// Recursive case
	children, _ := m.FS.GetChildren(n)
	i := 0
	for _, child := range children {
		i += countFiles(child)
	}
	return i
}
func countFilesLocal(path string) int {
	i := 0
	files, _ := ioutil.ReadDir(path)
	for _, file := range files {
		if !file.IsDir() {
			i++
		} else {
			i += countFilesLocal(path + string(os.PathSeparator) + file.Name())
		}
	}
	return i
}
