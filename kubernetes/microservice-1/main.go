package main

import (
	"context"
	"fmt"
	"log"
	"microservice1/pb"
	"net"
	"os"

	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

func main() {
	// Create a new gRPC server
	server := grpc.NewServer()
	pb.RegisterMicroservice1Server(server, Controller{})
	// Listen
	lis, err := net.Listen("tcp", "0.0.0.0"+":"+"4001")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	log.Println("Starting server at port 4001")
	log.Fatal(server.Serve(lis))
}

type Controller struct {
	pb.UnimplementedMicroservice1Server
}

func (c Controller) HelloWorld(context.Context, *emptypb.Empty) (*pb.Model, error) {
	msg := fmt.Sprintf("Hello world! > DB_HOST: %s | DB_USERNAME: %s | DB_PASSWORD: %s", os.Getenv("DB_HOST"), os.Getenv("DB_USERNAME"), os.Getenv("DB_PASSWORD"))
	return &pb.Model{Message: msg}, nil
}
