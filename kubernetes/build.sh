#!/bin/sh
# BUILD DOCKER IMAGES
docker build -t registry.gitlab.com/go2312514/examples-go/kubernetes/api-gateway ./api-gateway
docker build -t registry.gitlab.com/go2312514/examples-go/kubernetes/microservice1 ./microservice-1
# UPLOAD TO GITLAB CONTAINER REGISTRY
docker push registry.gitlab.com/go2312514/examples-go/kubernetes/api-gateway
docker push registry.gitlab.com/go2312514/examples-go/kubernetes/microservice1
