package main

import (
	"apigateway/pb"
	"context"
	"fmt"
	"log"
	"net/http"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/types/known/emptypb"
)

func helloHandler(w http.ResponseWriter, r *http.Request) {
	model, err := getMicroservice1()
	if err != nil {
		log.Println("error microservice1 grpc:", err)
	}
	fmt.Fprintf(w, model.Message)
}

func main() {
	http.HandleFunc("/", helloHandler)

	fmt.Println("Starting server at port 8000")
	log.Fatal(http.ListenAndServe("0.0.0.0:8000", nil))
}

func getMicroservice1() (*pb.Model, error) {
	// Set up a grpc connection to the server.
	conn, err := grpc.NewClient("microservice1:4001", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewMicroservice1Client(conn)

	// Get
	return c.HelloWorld(context.TODO(), &emptypb.Empty{})
}
