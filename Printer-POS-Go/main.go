package main

import (
	"bufio"
	"os"
	"time"

	"github.com/kenshaw/escpos"
)

func main() {
	Print()
}

func Print() {
	f, err := os.OpenFile("/dev/usb/lp0", os.O_RDWR, 0)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	w := bufio.NewWriter(f)
	p := escpos.New(bufio.NewReadWriter(nil, w))

	p.Init()
	p.SetSmooth(1)
	p.SetFont("B")
	p.SetAlign("center")
	p.SetFontSize(2, 1)
	p.Write(`LAVANDERIA TENDEDERO`)
	p.SetFont("A")
	p.SetFontSize(1, 1)
	p.Linefeed()
	p.Write(`C/ Don Juan de Austrias 162`)
	p.Linefeed()
	p.Write(`Roquetas de Mar`)
	p.Linefeed()
	p.Linefeed()
	p.Write(`Telefono    633384923`)
	p.Linefeed()
	p.Write(`CIF:       A-23492184`)

	p.Linefeed()
	p.Linefeed()

	p.Write("05/03/2024      18:15")
	p.Linefeed()
	p.Linefeed()
	p.SetAlign("center")
	p.Write("Ticket ID: 3241")
	p.Linefeed()
	p.Linefeed()

	p.Write("--------------------------")
	p.Linefeed()
	p.Linefeed()

	p.Write("Lavadora 4")
	p.Linefeed()
	p.Linefeed()
	p.Write("TOTAL:          1,00 euros")
	p.Linefeed()
	p.Write("Iva incluido              ")

	p.Linefeed()
	p.Linefeed()
	p.Write("----------------------------")
	p.Linefeed()
	p.Linefeed()

	p.Write("PAGO EN EFECTIVO")
	p.Linefeed()
	p.Write("Gracias por su visita!!!")

	p.Formfeed()

	// p.SetFont("B")
	// p.SetFontSize(1, 1)

	// p.SetEmphasize(1)
	// p.Write("halle")
	// p.Formfeed()

	// p.SetUnderline(1)
	// p.SetFontSize(4, 4)
	// p.Write("halle")

	// p.SetReverse(1)
	// p.SetFontSize(2, 4)
	// p.Write("halle")
	// p.Formfeed()

	// p.SetFont("C")
	// p.SetFontSize(8, 8)
	// p.Write("halle")
	// p.FormfeedN(5)

	// PRINT IMAGE
	// pngEscPos.PrintImage("./qr.png", f)

	p.Cut()
	p.End()
	w.Flush()

	time.Sleep(8 * time.Second)
}
