package cloudfunc

import (
	"context"
	"errors"
	"net/http"
	"os"
	"strings"

	"cloud.google.com/go/firestore"
)

type customContext struct {
	context.Context
	UserID string
}

func corsMiddleware(w http.ResponseWriter, r *http.Request) {
	// Set CORS headers for the preflight request
	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET,POST,HEAD,PUT,DELETE,PATCH")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Accept, Authorization")
		w.Header().Set("Access-Control-Max-Age", "3600")
		w.WriteHeader(http.StatusNoContent)
		return
	}
	// Set CORS headers for the main request.
	w.Header().Set("Access-Control-Allow-Origin", "*")
}
func authMiddleware(w http.ResponseWriter, r *http.Request) error {
	var err error = errors.New("error")
	// Auth Middleware
	w.Header().Set("Vary", "Authorization")
	authHeader := r.Header.Get("Authorization")

	if authHeader == "" {
		writeJSON(w, http.StatusUnauthorized, "invalid authorization header", nil)
		return err
	}

	headerParts := strings.Split(authHeader, " ")
	if len(headerParts) != 2 {
		writeJSON(w, http.StatusUnauthorized, "invalid authorization header", nil)
		return err
	}

	if headerParts[0] != "Bearer" {
		writeJSON(w, http.StatusUnauthorized, "unauthorized - no bearer", nil)
		return err
	}

	token := headerParts[1]
	userID, err := verifyIDToken(token)
	if err != nil {
		writeJSON(w, http.StatusUnauthorized, "invalid token", nil)
		return err
	}

	ctx := customContext{
		Context: r.Context(),
		UserID:  userID,
	}
	*r = *r.WithContext(ctx)

	return nil
}
func checkRoleMiddleware(userID string, laundryID string, rolesColl *firestore.CollectionRef) error {
	hasPermission := false
	requiredRole := os.Getenv("ROLE")

	doc, err := rolesColl.Doc(userID).Get(context.Background())
	if err != nil {
		return errors.New("no permission")
	}

	role, err := doc.DataAt("all")
	if err != nil {
		role, err = doc.DataAt(laundryID)
		if err == nil && role == requiredRole {
			hasPermission = true
		}
	} else if role == requiredRole {
		hasPermission = true
	}

	if !hasPermission {
		return errors.New("no permission")
	}

	return nil
}

func verifyIDToken(idToken string) (string, error) {
	token, err := authClient.VerifyIDToken(context.Background(), idToken)
	return token.UID, err
}
