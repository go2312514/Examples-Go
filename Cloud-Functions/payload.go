package cloudfunc

type updateUserRoleRequest struct {
	LaundryID string `json:"laundry_id"`
	UserID    string `json:"user_id"`
	Role      string `json:"role"`
}
