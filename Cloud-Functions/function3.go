package cloudfunc

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"strings"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go"
	"firebase.google.com/go/auth"
	"github.com/GoogleCloudPlatform/functions-framework-go/functions"
	"google.golang.org/api/option"
)

// consts
// const rootPathCloudFuncs = "./serverless_function_source_code"
const rolesCollectionName = "roles"

// vars
var projectID string
var credentialsJson []byte
var authClient *auth.Client

func init() {
	var err error
	// Read DB credentials
	projectID = os.Getenv("GCP_PROJECT_ID")
	credentialsJson, err = os.ReadFile(path.Join(rootPathCloudFuncs, "creds.json"))
	if err != nil {
		log.Fatal(err)
	}

	// Init firebase auth client
	opt := option.WithCredentialsJSON(credentialsJson)
	config := &firebase.Config{ProjectID: projectID}
	app, err := firebase.NewApp(context.Background(), config, opt)
	if err != nil {
		log.Fatal(err)
	}
	authClient, err = app.Auth(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	functions.HTTP("UpdateUserRole", updateUserRole)
}

// updateUserRole is an HTTP Cloud Function with a request parameter.
func updateUserRole(w http.ResponseWriter, r *http.Request) {

	// Middlewares
	corsMiddleware(w, r)
	err := authMiddleware(w, r)
	if err != nil {
		return
	}

	// Decode request
	var req updateUserRoleRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		writeJSON(w, http.StatusBadRequest, err.Error(), nil)
		return
	}

	// Connect DB
	ctx := r.Context().(customContext)
	client, err := firestore.NewClientWithDatabase(ctx, projectID, req.LaundryID, option.WithCredentialsJSON(credentialsJson))
	if err != nil {
		writeJSON(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	rolesColl := client.Collection(rolesCollectionName)

	// Check role middleware
	err = checkRoleMiddleware(ctx.UserID, req.LaundryID, client.Collection(rolesCollectionName))
	if err != nil {
		writeJSON(w, http.StatusForbidden, "user has no permission", nil)
		return
	}

	// Update role
	roleKey := "all"
	if req.Role != "superadmin" {
		roleKey = req.LaundryID
	}
	_, err = rolesColl.Doc(fmt.Sprint(req.UserID)).Set(ctx, map[string]string{roleKey: req.Role}, firestore.Merge(firestore.FieldPath{roleKey}))
	if err != nil {
		writeJSON(w, http.StatusInternalServerError, formatErr(err).Error(), nil)
		return
	}

	writeJSON(w, http.StatusOK, "user role was updated", nil)
}

// Remove sensitive information from err
func formatErr(err error) error {
	errStr := fmt.Sprint(err)
	if strings.Contains(errStr, "not found") {
		err = fmt.Errorf("document not found")
	} else if strings.Contains(errStr, "already exist") {
		err = fmt.Errorf("document already exists")
	}
	return err
}
