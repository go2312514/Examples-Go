package cloudfunc

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path"
	"strings"

	"cloud.google.com/go/firestore"
	"github.com/GoogleCloudPlatform/functions-framework-go/functions"
	"google.golang.org/api/option"
)

// consts
const rootPathCloudFuncs = "./serverless_function_source_code"
const machinesCollectionName = "machines"

func init() {

	// Serve function
	functions.HTTP("InitDatabase", initDatabase)
}

// initDatabase is an HTTP Cloud Function.
func initDatabase(w http.ResponseWriter, r *http.Request) {

	// Read DB credentials
	projectID := os.Getenv("GCP_PROJECT_ID")
	databaseID := os.Getenv("GCP_DATABASE_ID")
	credentialsJson, err := os.ReadFile(path.Join(rootPathCloudFuncs, "creds.json"))
	if err != nil {
		writeJSON(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	// Connect DB
	ctx := context.Background()
	client, err := firestore.NewClientWithDatabase(ctx, projectID, databaseID, option.WithCredentialsJSON(credentialsJson))
	if err != nil {
		writeJSON(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	machinesColl := client.Collection(machinesCollectionName)

	// Read machines json
	machinesJson, err := os.ReadFile(path.Join(rootPathCloudFuncs, "machines.json"))
	if err != nil {
		writeJSON(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	var machines []Machine
	err = json.Unmarshal(machinesJson, &machines)
	if err != nil {
		writeJSON(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	// Create machines
	for _, m := range machines {
		_, err = machinesColl.Doc(fmt.Sprint(m.ID)).Create(ctx, m)
		if err != nil {
			if !strings.Contains(err.Error(), "already exists") {
				writeJSON(w, http.StatusOK, "machines were already created: "+err.Error(), nil)
				return
			}
			writeJSON(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}
	}

	writeJSON(w, http.StatusCreated, "machines were created successfully", nil)
}
