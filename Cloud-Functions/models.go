package cloudfunc

type Machine struct {
	ID                 string  `json:"id" firestore:"id"`
	Label              string  `json:"label,omitempty" firestore:"label,omitempty"`
	Type               string  `json:"type,omitempty" firestore:"type,omitempty"`
	Weight             int64   `json:"weight_kg,omitempty" firestore:"weight_kg,omitempty"`
	Duration           int64   `json:"duration_sec,omitempty" firestore:"duration_sec,omitempty"`
	Price              float32 `json:"price_euro,omitempty" firestore:"price_euro,omitempty"`
	Pulses             []int   `json:"pulses,omitempty" firestore:"pulses,omitempty"` // only dryers
	ReadAddress        string  `json:"read_address,omitempty" firestore:"read_address,omitempty"`
	ReadRelay          int64   `json:"read_relay,omitempty" firestore:"read_relay,omitempty"`
	WriteAddress       string  `json:"write_address,omitempty" firestore:"write_address,omitempty"`
	WriteRelay         int64   `json:"write_relay,omitempty" firestore:"write_relay,omitempty"`
	PulseDurationMilli int64   `json:"pulse_duration_milli,omitempty" firestore:"pulse_duration_milli,omitempty"`
	PulseWaitMilli     int64   `json:"pulse_wait_milli,omitempty" firestore:"pulse_wait_milli,omitempty"`
	// Status             string    `json:"status,omitempty"`
	Hidden bool `json:"hidden,omitempty" firestore:"hidden,omitempty"`
}
