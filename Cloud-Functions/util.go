package cloudfunc

import (
	"encoding/json"
	"net/http"
)

// writeJSON is a utility function for writing JSON responses
func writeJSON(w http.ResponseWriter, status int, message string, data any) {
	// Set the Content-Type header to application/json
	w.Header().Set("Content-Type", "application/json")

	// Set the HTTP status code
	w.WriteHeader(status)

	// Construct the JSON response object
	response := map[string]any{
		"status":  status,
		"message": message,
		"data":    data,
	}

	// Encode the response object as JSON and write it to the response writer
	json.NewEncoder(w).Encode(response)
}
