package main

import (
	"fmt"

	gosseract "github.com/otiai10/gosseract/v2"
)

func main() {

	client := gosseract.NewClient()
	defer client.Close()
	client.SetLanguage("spa")
	client.SetImage("cv.jpg")
	text, err := client.Text()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(text)
}
