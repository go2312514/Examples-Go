package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

func main() {
	// HTTP endpoint
	url := "http://localhost:4000/api/v1/graphql/characters"

	// JSON body (graphQL query)
	query := map[string]string{
		"query": `
		{
			character(name: "Johan") {
				name
				class
				skills {
					name
					power
				}
			}
		}
	`}
	jsonQuery, err := json.Marshal(query)
	if err != nil {
		panic(err)
	}

	// Create a HTTP post request
	r, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonQuery))
	if err != nil {
		panic(err)
	}
	r.Header.Add("Content-Type", "application/json")

	// Do request
	client := &http.Client{}
	res, err := client.Do(r)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	// Read response and print
	body, err := io.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	// Print the JSON response.
	fmt.Println(string(body))
}
