package gql

import (
	"fmt"
	"graphql-api/models"

	"github.com/graphql-go/graphql"
)

var character = graphql.NewObject(
	graphql.ObjectConfig{Name: "character", Fields: graphql.Fields{
		"name":   &graphql.Field{Type: graphql.String},
		"class":  &graphql.Field{Type: graphql.String},
		"skills": &graphql.Field{Type: graphql.NewList(skill)},
	}},
)

var skill = graphql.NewObject(
	graphql.ObjectConfig{Name: "skill", Fields: graphql.Fields{
		"name":  &graphql.Field{Type: graphql.String},
		"power": &graphql.Field{Type: graphql.Int},
	}},
)

// Root query
var Query = graphql.NewObject(graphql.ObjectConfig{
	Name: "query",
	Fields: graphql.Fields{
		"character": &graphql.Field{
			Type:        character,
			Description: "Get single character",
			Args: graphql.FieldConfigArgument{
				"name": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				// Param
				name, nameOk := p.Args["name"].(string)

				if !nameOk {
					err := fmt.Errorf("invalid name parameter")
					return nil, err
				}

				var res models.Character
				// Begin database query
				for _, c := range models.Characters {
					if c.Name == name {
						res = c
					}
				}
				// End database query
				return res, nil
			},
		},

		"characters": &graphql.Field{
			Type:        graphql.NewList(character),
			Description: "List of characters filtered by params",
			Args: graphql.FieldConfigArgument{ // (query params)
				"class": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"skillName": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				// Params
				class, classOk := p.Args["class"].(string)
				skillName, skillNameOk := p.Args["skillName"].(string)

				res := make([]models.Character, 0)
				// Begin database query
				for _, c := range models.Characters {
					include := true
					if classOk && c.Class != class {
						include = false
					}
					if skillNameOk {
						hasSkillName := false
						for _, s := range c.Skills {
							if s.Name == skillName {
								hasSkillName = true
							}
						}
						if !hasSkillName {
							include = false
						}
					}

					if include {
						res = append(res, c)
					}
				}
				// End database query
				return res, nil
			},
		},
	},
})
