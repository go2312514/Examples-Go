package character

import (
	"github.com/gofiber/fiber/v2"
	"github.com/valyala/fasthttp/fasthttpadaptor"
)

func (c *Controller) GraphQLGet(ctx *fiber.Ctx) error {
	fastHttpHandler := fasthttpadaptor.NewFastHTTPHandler(c.GraphqlHandler)
	fastHttpHandler(ctx.Context())
	return nil
}
