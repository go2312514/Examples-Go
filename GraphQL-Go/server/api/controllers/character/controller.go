package character

import (
	"graphql-api/api/controllers/character/gql"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
)

type Controller struct {
	GraphqlHandler *handler.Handler
}

type ModelExample struct{}

func New() (*Controller, error) {
	// Setup graphQL schema and handler
	schema, err := graphql.NewSchema(graphql.SchemaConfig{Query: gql.Query})
	if err != nil {
		return nil, err
	}
	h := handler.New(&handler.Config{
		Schema:   &schema,
		Pretty:   true,
		GraphiQL: false,
	})

	// Create controller
	c := Controller{
		GraphqlHandler: h,
	}
	return &c, nil
}
