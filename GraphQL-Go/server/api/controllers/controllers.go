package controllers

import "graphql-api/api/controllers/character"

var (
	Character *character.Controller
	// ...
)

func Build() error {
	var err error
	Character, err = character.New()
	if err != nil {
		return err
	}
	return nil
}
