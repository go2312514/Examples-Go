package api

import (
	"graphql-api/api/controllers"
	"graphql-api/conf"
	"log"

	"github.com/goccy/go-json"
	"github.com/gofiber/fiber/v2"
)

var App *fiber.App

func Start() error {
	// Run server
	host := conf.Env.GetString("HOST")
	port := conf.Env.GetString("PORT")
	log.Printf("Starting server on http://%s:%s\n", host, port)
	return App.Listen(conf.Env.GetString("HOST") + ":" + conf.Env.GetString("PORT"))
}

func Setup() error {
	// Build controllers
	err := controllers.Build()
	if err != nil {
		return err
	}

	// Setup Fiber api server
	App = fiber.New(fiber.Config{
		JSONEncoder: json.Marshal,
		JSONDecoder: json.Unmarshal,
		BodyLimit:   conf.Conf.GetInt("apiBodyMaxSize"),
	})

	// Setup groups and middleware
	api := App.Group(conf.Conf.GetString("apiBasePath"))

	api.Post("/graphql/characters", controllers.Character.GraphQLGet)

	return nil
}
