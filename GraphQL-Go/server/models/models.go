package models

type Character struct {
	Name   string  `json:"name"`
	Class  string  `json:"class"`
	Skills []Skill `json:"skills"`
}

type Skill struct {
	Name  string `json:"name"`
	Power int    `json:"power"`
}

var Characters = []Character{
	{Name: "Johan", Class: "Mage", Skills: []Skill{{Name: "Fireball", Power: 100}}},
	{Name: "Reiner", Class: "Warrior", Skills: []Skill{{Name: "Stomp", Power: 300}, {Name: "Shield", Power: 80}}},
	{Name: "Aka", Class: "Assassin"},
}
