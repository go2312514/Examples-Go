/*
Copyright © 2022 Demetrio Navarro Martínez <deme1994@gmail.com>
*/
package cmd

import (
	"graphql-api/api"
	"graphql-api/conf"
	"log"
	"os"

	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "GraphQL-Api-Example",
	Short: "GraphQL-Api-Example",
	Long:  `Example of GraphQL using REST Api`,
	Run: func(cmd *cobra.Command, args []string) {

		log.SetFlags(log.LstdFlags | log.Lshortfile) // Set default log flags (print file and line)

		// Conf
		log.Println("Loading configuration...")
		err := conf.Setup("", "")
		if err != nil {
			log.Fatal("\033[31m"+"CONFIGURATION LOAD FAILED"+"\033[0m"+" -> ", err)
		}
		log.Println("\033[32m" + "CONFIGURATION LOADED" + "\033[0m")

		// API
		log.Println("Loading api...")
		err = api.Setup()
		if err != nil {
			log.Fatal("\033[31m"+"API SETUP FAILED"+"\033[0m"+" -> ", err)
		}
		log.Println("Starting server...")
		err = api.Start()
		if err != nil {
			log.Fatal("\033[31m"+"API FAILED"+"\033[0m"+" -> ", err)
		}
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.API-REST.yaml)")
}
