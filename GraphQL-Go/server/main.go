/*
Copyright © 2022 Demetrio Navarro Martínez <deme1994@gmail.com>
*/
package main

import "graphql-api/cmd"

func main() {
	cmd.Execute()
}
