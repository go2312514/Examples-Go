package api

import (
	"fmt"
	"grpc-go/api/controllers"
	"grpc-go/conf"
	"log"
	"net"

	"google.golang.org/grpc"
)

// var App *fiber.App
var server *grpc.Server

func Start() error {
	// Run server
	host := conf.Env.GetString("HOST")
	port := conf.Env.GetString("PORT")
	lis, err := net.Listen("tcp", host+":"+port)
	if err != nil {
		return fmt.Errorf("failed to listen: %v", err)
	}
	log.Println("Listening on ", host+":"+port)
	return server.Serve(lis)
}

func Setup() {
	// Create a new gRPC server
	server = grpc.NewServer()
	controllers.Build(server)

	// pvt.Get("/machines", controllers.Machine.GetAll)
	// pvt.Get("/machines/:id", controllers.Machine.Get)
	// pvt.Post("/machines", controllers.Machine.Insert)
	// pvt.Put("/machines/:id", controllers.Machine.Update)
	// pvt.Delete("/machines/:id", controllers.Machine.Delete)
}
