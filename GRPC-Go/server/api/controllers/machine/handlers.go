package machine

import (
	context "context"
	fmt "fmt"
	"grpc-go/api/controllers/machine/pb"
)

func (c *Controller) GetAll(in *pb.QueryParams, stream pb.Machine_GetAllServer) error {
	mtype := in.Mtype
	weight := int(in.Weight)

	fmt.Println("GetAll")
	fmt.Println("Mtype=", mtype, "Weight=", weight)

	all := []pb.Model{
		{ID: 1, Mtype: "Dryer", Weight: 21},
		{ID: 2, Mtype: "Washing Machine", Weight: 22},
		{ID: 3, Mtype: "Dryer", Weight: 23},
	}

	for _, m := range all {
		err := stream.Send(&m)
		if err != nil {
			return err
		}
	}

	return nil
}
func (c *Controller) Get(ctx context.Context, in *pb.ID) (*pb.Model, error) {
	id := int(in.ID)

	fmt.Println("Get")
	fmt.Println("ID=", id)

	return &pb.Model{ID: in.ID, Mtype: "Dryer", Weight: 22}, nil
}
func (c *Controller) Insert(ctx context.Context, in *pb.Model) (*pb.StatusResponse, error) {
	id := int(in.ID)
	mtype := in.Mtype
	weight := int(in.Weight)

	fmt.Println("Insert")
	fmt.Println("ID=", id, "Mtype=", mtype, "Weight=", weight)

	return &pb.StatusResponse{OK: true, Error: ""}, nil
}
func (c *Controller) Update(ctx context.Context, in *pb.Model) (*pb.StatusResponse, error) {
	id := int(in.ID)
	mtype := in.Mtype
	weight := int(in.Weight)

	fmt.Println("Update")
	fmt.Println("ID=", id, "Mtype=", mtype, "Weight=", weight)

	return &pb.StatusResponse{OK: true, Error: ""}, nil
}
func (c *Controller) Delete(ctx context.Context, in *pb.ID) (*pb.StatusResponse, error) {
	id := int(in.ID)

	fmt.Println("Delete")
	fmt.Println("ID=", id)

	return &pb.StatusResponse{OK: true, Error: ""}, nil
}
