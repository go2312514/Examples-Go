package machine

import (
	"grpc-go/api/controllers/machine/pb"

	grpc "google.golang.org/grpc"
)

type Controller struct {
	pb.UnimplementedMachineServer
	Model *ModelExample
}

type ModelExample struct{}

func New(s *grpc.Server) *Controller {
	c := Controller{}
	pb.RegisterMachineServer(s, &c)
	return &c
}
