package controllers

import (
	"grpc-go/api/controllers/machine"

	"google.golang.org/grpc"
)

var (
	Machine *machine.Controller
	// ...
)

func Build(s *grpc.Server) {
	Machine = machine.New(s)
}
