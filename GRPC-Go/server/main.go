/*
Copyright © 2022 Demetrio Navarro Martínez <deme1994@gmail.com>
*/
package main

import "grpc-go/cmd"

func main() {
	cmd.Execute()
}
