package main

import (
	"context"
	"io"
	"log"

	machine "clientgrpc/pb"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial("localhost:4000", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := machine.NewMachineClient(conn)

	// GetAll
	stream, err := c.GetAll(context.TODO(), &machine.QueryParams{Mtype: "Dryer", Weight: 32})
	if err != nil {
		log.Fatalf("could not insert: %v", err)
	}
	for {
		model, err := stream.Recv()
		if err == io.EOF {
			// Reached the end of the stream
			break
		} else if err != nil {
			log.Fatal(err)
		}

		log.Println("ID=", model.ID, "Mtype=", model.Mtype, "Weight=", model.Weight)
	}

	// Get
	res1, err := c.Get(context.TODO(), &machine.ID{ID: 2})
	if err != nil {
		log.Fatalf("could not insert: %v", err)
	}
	log.Println("ID=", res1.ID, "Mtype=", res1.Mtype, "Weight=", res1.Weight)

	// Insert
	res2, err := c.Insert(context.TODO(), &machine.Model{ID: 2, Mtype: "Dryer", Weight: 32})
	if err != nil {
		log.Fatalf("could not insert: %v", err)
	}
	log.Printf("Status: %t, Error: %s", res2.GetOK(), res2.GetError())

	// Update
	res3, err := c.Update(context.TODO(), &machine.Model{ID: 2, Mtype: "Dryer", Weight: 32})
	if err != nil {
		log.Fatalf("could not insert: %v", err)
	}
	log.Printf("Status: %t, Error: %s", res3.GetOK(), res3.GetError())

	// Delete
	res4, err := c.Delete(context.TODO(), &machine.ID{ID: 2})
	if err != nil {
		log.Fatalf("could not insert: %v", err)
	}
	log.Printf("Status: %t, Error: %s", res4.GetOK(), res4.GetError())
}
