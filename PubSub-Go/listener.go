package main

import (
	"context"
	"fmt"
	"pubsub-go/model"

	"cloud.google.com/go/pubsub"
	"google.golang.org/protobuf/proto"
)

func listenSubscription() error {
	// Use a callback to receive messages via subscription
	sub := client.Subscription("CREATE_ADDRESS_SUB")
	err := sub.Receive(context.TODO(), func(ctx context.Context, msg *pubsub.Message) {
		// Get input data
		in := model.AddressBook{}
		proto.Unmarshal(msg.Data, &in)
		msg.Ack() // Acknowledge that we've consumed the message.

		fmt.Println(in.People[0].Email)

		// Check required fields
		// err := utilities.CheckRequiredFields(in.Service, in.CorrelationId)
		// if err != nil {
		// 	fmt.Println(err)
		// 	return
		// }
	})
	return err
}
