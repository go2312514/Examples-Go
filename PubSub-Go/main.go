package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"pubsub-go/model"
	"strings"
	"time"

	"cloud.google.com/go/pubsub"
	"google.golang.org/api/option"
	"google.golang.org/protobuf/proto"
)

var client *pubsub.Client
var topics map[string]*pubsub.Topic

func main() {
	err := Setup()
	if err != nil {
		log.Fatal(err)
	}

	// Listen subscription
	go listenSubscription()

	// Publish data
	err = publishData()
	if err != nil {
		log.Fatal(err)
	}

	time.Sleep(8 * time.Second)
}

func Setup() error {
	var err error
	client, err = pubsub.NewClient(context.TODO(), os.Getenv("GCP_PROJECT_ID"), option.WithCredentialsJSON([]byte(os.Getenv("GCP_CREDENTIALS"))))
	if err != nil {
		return err
	}

	err = createTopicsAndSubs()
	if err != nil {
		return err
	}

	return nil
}

func createTopicsAndSubs() error {
	client.CreateTopic(context.TODO(), "CREATE_ADDRESS")
	t := client.Topic("CREATE_ADDRESS")
	_, err := client.CreateSubscription(context.TODO(), "CREATE_ADDRESS_SUB",
		pubsub.SubscriptionConfig{Topic: t})
	if !strings.Contains(fmt.Sprint(err), "AlreadyExists") {
		return err
	}

	return nil
}

func publishData() error {
	topics["CREATE_ADDRESS"] = client.Topic("CREATE_ADDRESS")

	address := model.AddressBook{
		People: []*model.Person{
			{
				Email: "email@gmail.com",
			},
		},
	}

	data, err := proto.Marshal(&address)
	if err != nil {
		return err
	}

	topics["CREATE_ADDRESS"].Publish(context.TODO(), &pubsub.Message{
		Data: data,
	})

	return nil
}
