package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Define a struct to hold client information
type Client struct {
	ID         string
	Connection *websocket.Conn
}

// Keep track of connected clients
var clients = make(map[string]*Client)

func main() {
	http.HandleFunc("/ws", handleConnections)
	fmt.Println("Server is listening...")
	log.Fatal(http.ListenAndServe(":8000", nil))
}

func handleConnections(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer ws.Close()

	// Get the client ID from the query parameter (e.g., /ws?id=alice)
	clientID := r.URL.Query().Get("id")
	if clientID == "" {
		clientID = "anonymous" // Default ID if not provided
	}

	destinationID := r.URL.Query().Get("destinationId")
	if destinationID == "" {
		destinationID = "anonymous" // Default ID if not provided
	}

	// Create a new client and add it to the clients map
	client := &Client{ID: clientID, Connection: ws}
	clients[clientID] = client
	fmt.Println("New client connected:", clientID)

	for {
		// Read message from browser
		mt, message, err := ws.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			delete(clients, clientID) // Remove client from the map
			fmt.Println("Client disconnected:", clientID)
			break
		}

		// Assuming the message format is "destination:message"
		msg := string(message)

		// Send the message to the destination client
		if destClient, ok := clients[destinationID]; ok {
			err = destClient.Connection.WriteMessage(mt, []byte(msg))
			if err != nil {
				log.Println("write:", err)
				break
			}
		} else {
			log.Println("Destination client not found:", destinationID)
		}
	}
}
