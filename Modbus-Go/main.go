package main

import (
	"log"
	"time"

	"github.com/simonvetter/modbus"
)

func main() {
	var client *modbus.ModbusClient
	var err error

	// for an RTU (serial) device/bus
	client, err = modbus.NewClient(&modbus.ClientConfiguration{
		URL:      "rtu:///dev/ttyUSB0",
		Speed:    9600,
		DataBits: 8,
		Parity:   modbus.PARITY_NONE,
		StopBits: 1,
		Timeout:  1 * time.Second,
	})
	if err != nil {
		log.Fatal(err)
	}

	err = client.Open()
	if err != nil {
		log.Fatal(err)
	}

	// Switch to unit ID (a.k.a. slave ID) #4
	client.SetUnitId(1)

	err = client.WriteRegister(1, 256)
	if err != nil {
		log.Fatal(err)
	}
	time.Sleep(1 * time.Second)
	err = client.WriteRegister(1, 512)
	if err != nil {
		log.Fatal(err)
	}

	client.Close()
}
