package monitor

import (
	"API-REST/services/conf"
	"fmt"
	"net"

	"github.com/gofiber/fiber/v2"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var registry *prometheus.Registry
var requestCalls = promauto.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_requests_total",
		Help: "The total number of request calls",
	},
	[]string{"method", "path", "status", "user"},
)

func Setup() error {
	// Check Prometheus is online
	ip := conf.Env.GetString("PROM_HOST")
	port := conf.Env.GetString("PROM_PORT")

	conn, err := net.Dial("tcp", ip+":"+port)
	if err != nil {
		return err
	}
	defer conn.Close()

	// Register metrics
	registry = prometheus.NewRegistry()
	err = registry.Register(requestCalls)
	if err != nil {
		return err
	}

	return nil
}

func AddRequestCall(ctx *fiber.Ctx) {
	userID := ""
	if claimerID := ctx.Locals("Claimer-ID"); claimerID != nil {
		userID = claimerID.(string)
	}
	requestCalls.WithLabelValues(
		ctx.Route().Method,
		ctx.Route().Path,
		fmt.Sprint(ctx.Context().Response.StatusCode()),
		userID,
	).Inc()
}
