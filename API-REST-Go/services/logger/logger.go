package logger

import (
	"API-REST/services/conf"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/natefinch/lumberjack"
)

var Logger *log.Logger

func Setup() error {
	// Read conf
	dir := conf.Conf.GetString("log.dir")
	filename := conf.Conf.GetString("log.fileName")
	ext := conf.Conf.GetString("log.fileExt")
	maxSizeMB := conf.Conf.GetInt("log.maxSizeMB")

	// Default logger if not specified is stdout
	if dir == "" || filename == "" {
		Logger = log.New(os.Stdout, "", log.Ldate|log.Ltime|log.LstdFlags|log.Lshortfile)
		return nil
	}
	// Create directory if not exist
	err := os.Mkdir(dir, os.ModePerm)
	if err != nil && !strings.Contains(fmt.Sprint(err), "file exists") {
		return err
	}

	// Create logger
	path := dir + "/" + filename + "." + ext
	Logger = log.New(&lumberjack.Logger{
		Filename: path,
		MaxSize:  maxSizeMB, // megabytes after which new file is created
		//MaxBackups: 3,  // number of backups
		//MaxAge:     28, //days
	}, "", log.Ldate|log.Ltime|log.LstdFlags|log.Lshortfile)

	return nil
}

func GetAll() ([]string, error) {
	dir := conf.Conf.GetString("log.dir")
	ext := conf.Conf.GetString("log.fileExt")

	// List the files in the directory
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}
	// Append the names of the files
	filenames := make([]string, 0)
	for _, file := range files {
		if strings.Contains(file.Name(), ext) {
			filenames = append(filenames, file.Name())
		}
	}

	return filenames, nil
}

func Get(filename string) ([]string, error) {
	dir := conf.Conf.GetString("log.dir")

	// Read the content of the file
	path := dir + "/" + filename
	content, err := ioutil.ReadFile(path)
	if err != nil {
		if err != nil {
			return nil, err
		}
	}

	lines := strings.Split(string(content), "\n")
	lines = lines[:len(lines)-1]
	return lines, nil
}

func Delete(filename string) error {
	dir := conf.Conf.GetString("log.dir")

	// Remove the file
	path := dir + "/" + filename
	err := os.Remove(path)
	if err != nil {
		return err
	}

	return nil
}
