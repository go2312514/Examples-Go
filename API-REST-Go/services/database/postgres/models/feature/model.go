package feature

import (
	"API-REST/services/database/postgres/utils"
	"database/sql"
	"errors"
)

type Model struct {
	DB *sql.DB
}

func New(db *sql.DB) (*Model, error) {
	exists, err := utils.CheckIfTableExists(db, "public", "features")
	if err != nil {
		return nil, err
	}
	if !exists {
		err = errors.New("table features doesn't exist in db")
		return nil, err
	}
	return &Model{DB: db}, nil
}
