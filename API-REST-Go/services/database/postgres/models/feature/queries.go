package feature

import (
	"API-REST/services/database/postgres/utils"
	"strings"
	"time"

	"github.com/cridenour/go-postgis"
	"github.com/google/uuid"
)

type QueryParams struct {
	Page       *int
	PageSize   *int
	FromDate   *time.Time
	ToDate     *time.Time
	UserID     *uuid.UUID
	MostRecent *bool
}

func (m *Model) GetAll(params *QueryParams) ([]*Feature, error) {
	features := make([]*Feature, 0)
	// Set query params
	limit, offset, err := utils.GetLimitAndOffset(params.Page, params.PageSize)
	if err != nil {
		return nil, err
	}
	var whereQueryParams []string
	joinMostRecent := ""

	if params.MostRecent != nil && *params.MostRecent {
		joinMostRecent =
			` INNER JOIN (
				SELECT user_id, MAX(timestamp) AS max_timestamp
				FROM features
				GROUP BY user_id
			) AS t2 ON t.user_id = t2.user_id AND t.timestamp = t2.max_timestamp`

	} else if params.FromDate != nil && params.ToDate != nil {
		whereQueryParams = append(whereQueryParams,
			"AND t.timestamp >= '"+params.FromDate.Format("2006-01-02T15:04:05Z")+
				"' AND t.timestamp <= '"+params.ToDate.Format("2006-01-02T15:04:05Z")+"'")
	}
	if params.UserID != nil {
		whereQueryParams = append(whereQueryParams,
			"AND t.user_id = '"+params.UserID.String()+"'")
	}

	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		GeomFromEWKB(t.geom) AS geom,
		t.timestamp,
		t.user_id
	FROM features AS t ` +
		joinMostRecent +
		` WHERE 1=1 ` + strings.Join(whereQueryParams, " ") +
		`LIMIT $1 OFFSET $2`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	rows, err := stmt.Query(limit, offset)
	if err != nil {
		return nil, err
	}

	// Iterate over the rows
	for rows.Next() {
		var f Feature
		f.Geom = &postgis.PointS{}
		err := rows.Scan(
			&f.Geom,
			&f.Timestamp,
			&f.UserID,
		)
		if err != nil {
			return nil, err
		}
		features = append(features, &f)
	}

	return features, nil
}
func (m *Model) Insert(f *Feature) error {
	_, err := m.DB.Exec(`
	INSERT INTO features(geom, timestamp, user_id)
	VALUES(GeomFromEWKB($1), $2, $3);
    `, *f.Geom, f.Timestamp, f.UserID.String())
	if err != nil {
		return err
	}
	return nil
}
