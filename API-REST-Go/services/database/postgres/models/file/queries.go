package file

import (
	"API-REST/services/database/postgres/utils"
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"
)

type QueryParams struct {
	Page            *int
	PageSize        *int
	Any             *string
	FromDate        *time.Time
	ToDate          *time.Time
	Name            *string
	ContentType     *string
	Extension       *string
	SizeGreaterThan *int
	SizeLessThan    *int
	Storage         *string
}

func (m *Model) GetAll(params *QueryParams) ([]*File, error) {
	files := make([]*File, 0)
	// Set query params
	limit, offset, err := utils.GetLimitAndOffset(params.Page, params.PageSize)
	if err != nil {
		return nil, err
	}
	var whereQueryParams []string
	if params.Any != nil {
		like := "'%" + *params.Any + "%'"
		whereQueryParams = append(whereQueryParams,
			"AND (name LIKE "+like+
				" or content_type LIKE "+like+
				" or extension LIKE "+like+
				")")
	}
	if params.FromDate != nil && params.ToDate != nil {
		whereQueryParams = append(whereQueryParams,
			"AND created_at >= "+params.FromDate.Format("2006-01-02T15:04:05Z")+
				" AND created_at <= "+params.ToDate.Format("2006-01-02T15:04:05Z"))
	}
	if params.Name != nil {
		whereQueryParams = append(whereQueryParams,
			"AND name LIKE '%"+*params.Name+"%'")
	}
	if params.ContentType != nil {
		whereQueryParams = append(whereQueryParams,
			"AND content_type = '"+*params.ContentType+"'")
	}
	if params.Extension != nil {
		whereQueryParams = append(whereQueryParams,
			"AND extension = '"+*params.Extension+"'")
	}
	if params.SizeGreaterThan != nil {
		whereQueryParams = append(whereQueryParams,
			"AND size >= "+fmt.Sprint(*params.SizeGreaterThan))
	}
	if params.SizeLessThan != nil {
		whereQueryParams = append(whereQueryParams,
			"AND size <= "+fmt.Sprint(*params.SizeLessThan))
	}
	if params.Storage != nil {
		whereQueryParams = append(whereQueryParams,
			"AND storage = '"+*params.Storage+"'")
	}

	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		id,
		name,
		path,
		content_type,
		extension,
		size,
		storage
	FROM files
	WHERE uploaded = TRUE ` + strings.Join(whereQueryParams, " ") +
		`LIMIT $1 OFFSET $2`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	rows, err := stmt.Query(limit, offset)
	if err != nil {
		return nil, err
	}

	// Iterate over the rows
	for rows.Next() {
		var f File
		err := rows.Scan(
			&f.ID,
			&f.Name,
			&f.Path,
			&f.ContentType,
			&f.Extension,
			&f.Size,
			&f.Storage,
		)
		if err != nil {
			return nil, err
		}
		files = append(files, &f)
	}

	return files, nil
}
func (m *Model) Get(id int) (*File, error) {
	var f File
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		name,
		path,
		content_type,
		extension,
		size,
		storage
	FROM files
	WHERE id = $1
	AND uploaded = TRUE`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	f.ID = id
	err = stmt.QueryRow(id).Scan(
		&f.Name,
		&f.Path,
		&f.ContentType,
		&f.Extension,
		&f.Size,
		&f.Storage,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("file not found")
		} else {
			return nil, err
		}
	}

	return &f, nil
}
func (m *Model) GetOrphaned() ([]*File, error) {
	var files []*File
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		id,
		name,
		path,
		storage
	FROM files
	WHERE uploaded = FALSE
	`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	rows, err := stmt.Query()
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("files not found")
		} else {
			return nil, err
		}
	}

	// Build files
	// Iterate over the rows
	for rows.Next() {
		var f File
		err := rows.Scan(
			&f.ID,
			&f.Name,
			&f.Path,
			&f.Storage,
		)
		if err != nil {
			return nil, err
		}
		files = append(files, &f)
	}

	return files, nil
}
func (m *Model) GetExtensions() ([]string, error) {
	extensions := make([]string, 0)

	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		extension
	FROM files
	WHERE uploaded = TRUE 
	GROUP BY extension
	`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}

	// Iterate over the rows
	for rows.Next() {
		var extension string
		err := rows.Scan(
			&extension,
		)
		if err != nil {
			return nil, err
		}
		extensions = append(extensions, extension)
	}

	return extensions, nil
}
func (m *Model) Insert(f *File) (int, error) {
	var id int
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	INSERT INTO files
	(name, path, content_type, extension, size, storage) VALUES ($1,$2,$3,$4,$5,$6)
	RETURNING id`)
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	// Execute query
	err = stmt.QueryRow(f.Name, f.Path, f.ContentType, f.Extension, f.Size, f.Storage).Scan(&id)

	return id, err
}
func (m *Model) SetUploaded(id int) error {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	UPDATE files
	SET uploaded = 'true'
	WHERE id = $1`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(id)

	return err
}
func (m *Model) Delete(id int) error {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	DELETE from files 
	WHERE id = $1`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(id)

	return err
}
