package permission

import (
	"API-REST/services/database/postgres/utils"
	"database/sql"
	"errors"
	"strings"
)

type QueryParams struct {
	Page      *int
	PageSize  *int
	Resource  *string
	Operation *string
}

func (m *Model) GetAll(params *QueryParams) ([]*Permission, error) {
	permissions := make([]*Permission, 0)
	// Set query params
	limit, offset, err := utils.GetLimitAndOffset(params.Page, params.PageSize)
	if err != nil {
		return nil, err
	}
	var whereQueryParams []string
	if params.Resource != nil {
		whereQueryParams = append(whereQueryParams,
			"AND resource = '"+*params.Resource+"'")
	}
	if params.Operation != nil {
		whereQueryParams = append(whereQueryParams,
			"AND operation = '"+*params.Operation+"'")
	}

	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		id,
		resource,
		operation
	FROM permissions
	WHERE 1=1 ` + strings.Join(whereQueryParams, " ") +
		`LIMIT $1 OFFSET $2`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	rows, err := stmt.Query(limit, offset)
	if err != nil {
		return nil, err
	}

	// Iterate over the rows
	for rows.Next() {
		var r Permission
		err := rows.Scan(
			&r.ID,
			&r.Resource,
			&r.Operation,
		)
		if err != nil {
			return nil, err
		}
		permissions = append(permissions, &r)
	}

	return permissions, nil
}
func (m *Model) Get(id int) (*Permission, error) {
	var p Permission
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		resource,
		operation
	FROM permissions
	WHERE id = $1`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	p.ID = id
	err = stmt.QueryRow(id).Scan(
		&p.Resource,
		&p.Operation,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("permission not found")
		} else {
			return nil, err
		}
	}

	return &p, nil
}

// func (m *Model) Insert(p *Permission) error {
// 	err := m.Db.Table("permissions").Insert(map[string]interface{}{
// 		"resource":  strings.ToLower(p.Resource),
// 		"operation": strings.ToLower(p.Operation),
// 	})
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }
// func (m *Model) Update(p *Permission) error {
// 	_, err := m.Db.Table("permissions").Where("id", "=", p.ID).Update(map[string]interface{}{
// 		"resource":  strings.ToLower(p.Resource),
// 		"operation": strings.ToLower(p.Operation),
// 	})
// 	return err
// }
// func (m *Model) Delete(id int) error {
// 	_, err := m.Db.Table("permissions").Where("id", "=", id).Delete()
// 	return err
// }
