package role

import (
	"API-REST/services/database/postgres/models/permission"
	"API-REST/services/database/postgres/utils"
	"database/sql"
	"errors"
	"fmt"
	"strings"
)

type QueryParams struct {
	Page     *int
	PageSize *int
	Name     *string
}

func (m *Model) GetAll(params *QueryParams) ([]*Role, error) {
	roles := make([]*Role, 0)
	// Set query params
	limit, offset, err := utils.GetLimitAndOffset(params.Page, params.PageSize)
	if err != nil {
		return nil, err
	}
	var whereQueryParams []string
	if params.Name != nil {
		whereQueryParams = append(whereQueryParams,
			"AND name = '"+*params.Name+"'")
	}

	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		id,
		name
	FROM roles
	WHERE name != 'superadmin' ` + strings.Join(whereQueryParams, " ") +
		`LIMIT $1 OFFSET $2`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	rows, err := stmt.Query(limit, offset)
	if err != nil {
		return nil, err
	}

	// Iterate over the rows
	for rows.Next() {
		var r Role
		err := rows.Scan(
			&r.ID,
			&r.Name,
		)
		if err != nil {
			return nil, err
		}
		roles = append(roles, &r)
	}

	return roles, nil
}
func (m *Model) Get(id int) (*Role, error) {
	var r Role
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		name,
		permissions.id AS permission_id,
		permissions.resource AS permission_resource,
		permissions.operation AS permission_operation
	FROM roles
	LEFT JOIN roles_permissions ON roles.id = roles_permissions.role_id
	LEFT JOIN permissions ON roles_permissions.permission_id = permissions.id
	WHERE roles.id = $1
	AND roles.name != 'superadmin'
	`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	rows, err := stmt.Query(id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("role not found")
		} else {
			return nil, err
		}
	}

	// Build role and append permissions
	r.ID = id
	// Iterate over the rows (one per permission)
	for rows.Next() {
		var permissionID int
		var permissionResource string
		var permissionOperation string
		err := rows.Scan(
			&r.Name,
			&permissionID,
			&permissionResource,
			&permissionOperation,
		)
		if err != nil {
			return nil, err
		}
		r.Permissions = append(r.Permissions, &permission.Permission{
			ID:        permissionID,
			Resource:  permissionResource,
			Operation: permissionOperation,
		})
	}

	return &r, nil
}
func (m *Model) Insert(r *Role) (int, error) {
	var id int
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	INSERT INTO roles
	(name) VALUES ($1)
	RETURNING id`)
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	// Execute query
	err = stmt.QueryRow(strings.ToLower(r.Name)).Scan(&id)

	return id, err
}
func (m *Model) Update(r *Role) error {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	UPDATE roles SET 
		name = $1,
	WHERE id = $2 AND name != 'superadmin'`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(strings.ToLower(r.Name), r.ID)

	return err
}
func (m *Model) UpdatePermissions(id int, permissionIDs ...int) error {
	if len(permissionIDs) == 0 {
		return errors.New("no permission id parameter was given")
	}

	// Begin transaction
	tx, err := m.DB.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	// REMOVE ALL PERMISSIONS (CLEAR)
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	DELETE FROM roles_permissions
	WHERE role_id = $1
	AND roles.name != 'superadmin';`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(id)
	if err != nil {
		return err
	}

	// ADD NEW PERMISSIONS
	// Prepare statement
	var values []string
	for _, permID := range permissionIDs {
		v := fmt.Sprint(`(`, id, `,`, permID, `)`)
		values = append(values, v)
	}
	stmt, err = m.DB.Prepare(`
	INSERT INTO role_permissions (role_id, permission_id)
	VALUES ` + strings.Join(values, ","))
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(id)
	if err != nil {
		return err
	}

	// Commit
	return tx.Commit()
}
func (m *Model) Delete(id int) error {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	DELETE from roles 
	WHERE id = $1`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(id)

	return err
}
