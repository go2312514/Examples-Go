package role

import (
	"API-REST/services/database/postgres/utils"
	"database/sql"
	"errors"
)

type Model struct {
	DB *sql.DB
}

func New(db *sql.DB) (*Model, error) {
	exists, err := utils.CheckIfTableExists(db, "public", "roles")
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.New("table roles doesn't exist in db")
	}
	return &Model{DB: db}, nil
}
