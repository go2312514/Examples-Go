package user

import (
	"API-REST/services/database/postgres/models/file"
	"API-REST/services/database/postgres/models/role"
	"time"

	"github.com/google/uuid"
)

type User struct {
	ID        uuid.UUID  `json:"id" csv:"-"`
	CreatedAt time.Time  `json:"created_at" csv:"-"`
	UpdatedAt *time.Time `json:"updated_at,omitempty" csv:"-"`
	DeletedAt *time.Time `json:"deleted_at,omitempty" csv:"-"`

	Username string `json:"username" csv:"username"`
	Email    string `json:"email" csv:"email"`
	Password string `json:"-" csv:"-"`

	Nick      string  `json:"nick" csv:"nick"`
	FirstName *string `json:"first_name,omitempty" csv:"first_name"`
	LastName  *string `json:"last_name,omitempty" csv:"last_name"`
	Phone     *string `json:"phone,omitempty" csv:"phone"`
	Address   *string `json:"address,omitempty" csv:"address"`

	LastLogin          *time.Time `json:"last_login,omitempty" csv:"-"`
	LastPasswordChange *time.Time `json:"last_password_change,omitempty" csv:"-"`
	VerifiedEmail      bool       `json:"verified_email,omitempty" csv:"-"`
	VerifiedPhone      bool       `json:"verified_phone,omitempty" csv:"-"`
	BanDate            *time.Time `json:"ban_date,omitempty" csv:"-"`
	BanExpire          *time.Time `json:"ban_expire,omitempty" csv:"-"`

	Roles []*role.Role `json:"roles,omitempty" csv:"-"`

	Photo *file.File `json:"photo,omitempty" csv:"-"`
	CV    *file.File `json:"cv,omitempty" csv:"-"`
	// ...
}
