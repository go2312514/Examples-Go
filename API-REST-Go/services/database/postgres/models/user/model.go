package user

import (
	"API-REST/services/database/postgres/utils"
	"database/sql"
	"errors"
)

type Model struct {
	DB *sql.DB
}

func New(db *sql.DB) (*Model, error) {
	exists, err := utils.CheckIfTableExists(db, "public", "users")
	if err != nil {
		return nil, err
	}
	if !exists {
		err = errors.New("table users doesn't exist in db")
		return nil, err
	}
	return &Model{DB: db}, nil
}
