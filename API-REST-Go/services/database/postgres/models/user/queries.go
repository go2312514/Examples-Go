package user

import (
	"API-REST/services/database/postgres/models/permission"
	"API-REST/services/database/postgres/models/role"
	"API-REST/services/database/postgres/utils"
	"database/sql"
	"errors"
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/google/uuid"
)

type QueryParams struct {
	Page     *int
	PageSize *int
	Any      *string
	Deleted  *bool
	Banned   *bool
	Year     *int
}

func (m *Model) GetAll(params *QueryParams) ([]*User, error) {
	users := make([]*User, 0)
	// Set query params
	limit, offset, err := utils.GetLimitAndOffset(params.Page, params.PageSize)
	if err != nil {
		return nil, err
	}
	var whereQueryParams []string
	if params.Any != nil {
		like := "'%" + *params.Any + "%'"
		whereQueryParams = append(whereQueryParams,
			"AND (username LIKE "+like+
				" or email LIKE "+like+
				" or nick LIKE "+like+
				" or address LIKE "+like+
				" or first_name LIKE "+like+
				" or last_name LIKE "+like+")")
	}
	if params.Year != nil {
		startDate := fmt.Sprint(*params.Year, "-01-01")
		endDate := fmt.Sprint(*params.Year, "-12-31")
		whereQueryParams = append(whereQueryParams,
			"AND created_at >= "+startDate+" AND created_at <= "+endDate)
	}
	if params.Deleted != nil {
		if *params.Deleted {
			whereQueryParams = append(whereQueryParams,
				"AND deleted_at IS NOT NULL")
		} else {
			whereQueryParams = append(whereQueryParams,
				"AND deleted_at IS NULL")
		}
	}
	if params.Banned != nil {
		if *params.Banned {
			whereQueryParams = append(whereQueryParams,
				"AND ban_date IS NOT NULL")
		} else {
			whereQueryParams = append(whereQueryParams,
				"AND ban_date IS NULL")
		}
	}

	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		id,
		username,
		email,
		nick,
		first_name,
		last_name,
		phone,
		address
	FROM users
	WHERE 1=1 ` + strings.Join(whereQueryParams, " ") +
		`LIMIT $1 OFFSET $2`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	rows, err := stmt.Query(limit, offset)
	if err != nil {
		return nil, err
	}

	// Iterate over the rows
	for rows.Next() {
		var u User
		err := rows.Scan(
			&u.ID,
			&u.Username,
			&u.Email,
			&u.Nick,
			&u.FirstName,
			&u.LastName,
			&u.Phone,
			&u.Address,
		)
		if err != nil {
			return nil, err
		}
		users = append(users, &u)
	}

	return users, nil
}
func (m *Model) Get(id uuid.UUID) (*User, error) {
	var u User
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		created_at, 
		updated_at, 
		deleted_at, 
		username, 
		email, 
		nick, 
		first_name, 
		last_name, 
		phone, 
		address, 
		last_login, 
		last_password_change, 
		verified_email, 
		verified_phone, 
		ban_date, 
		ban_expire, 
		roles.id AS role_id, 
		roles.name AS role_name 
	FROM users 
	LEFT JOIN users_roles ON users.id = users_roles.user_id 
	LEFT JOIN roles ON users_roles.role_id = roles.id 
	WHERE users.id = $1`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	rows, err := stmt.Query(id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("user not found")
		} else {
			return nil, err
		}
	}

	// Build user and append roles
	u.ID = id
	// Iterate over the rows (one per role)
	for rows.Next() {
		var roleID int
		var roleName string
		err := rows.Scan(
			&u.CreatedAt,
			&u.UpdatedAt,
			&u.DeletedAt,
			&u.Username,
			&u.Email,
			&u.Nick,
			&u.FirstName,
			&u.LastName,
			&u.Phone,
			&u.Address,
			&u.LastLogin,
			&u.LastPasswordChange,
			&u.VerifiedEmail,
			&u.VerifiedPhone,
			&u.BanDate,
			&u.BanExpire,
			&roleID,
			&roleName,
		)
		if err != nil {
			return nil, err
		}
		u.Roles = append(u.Roles, &role.Role{ID: roleID, Name: roleName})
	}

	return &u, nil
}
func (m *Model) GetIDByEmail(email string) (uuid.UUID, error) {
	var id uuid.UUID
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		id 
	FROM users
	WHERE email = $1`)
	if err != nil {
		return uuid.Nil, err
	}
	defer stmt.Close()

	// Execute the query
	err = stmt.QueryRow(strings.ToLower(email)).Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			return uuid.Nil, errors.New("user not found")
		} else {
			return uuid.Nil, err
		}
	}

	return id, nil
}
func (m *Model) GetPassword(id uuid.UUID) (string, error) {
	var password string
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		password 
	FROM users
	WHERE id = $1`)
	if err != nil {
		return "", err
	}
	defer stmt.Close()

	// Execute the query
	err = stmt.QueryRow(id).Scan(&password)
	if err != nil {
		if err == sql.ErrNoRows {
			return "", errors.New("user not found")
		} else {
			return "", err
		}
	}

	return password, nil
}
func (m *Model) GetByEmail(email string) (*User, error) {
	var u User
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		id,
		created_at,
		deleted_at,
		username,
		last_login,
		last_password_change,
		ban_date,
		ban_expire 
	FROM users
	WHERE email = $1`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	err = stmt.QueryRow(strings.ToLower(email)).Scan(
		&u.ID,
		&u.CreatedAt,
		&u.DeletedAt,
		&u.Username,
		&u.LastLogin,
		&u.LastPasswordChange,
		&u.BanDate,
		&u.BanExpire,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("user not found")
		} else {
			return nil, err
		}
	}

	return &u, nil
}
func (m *Model) GetByEmailWithPassword(email string) (*User, error) {
	var u User
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		id,
		created_at,
		deleted_at,
		username,
		password,
		last_login,
		last_password_change,
		ban_date,
		ban_expire 
	FROM users
	WHERE email = $1`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	err = stmt.QueryRow(strings.ToLower(email)).Scan(
		&u.ID,
		&u.CreatedAt,
		&u.DeletedAt,
		&u.Username,
		&u.Password,
		&u.LastLogin,
		&u.LastPasswordChange,
		&u.BanDate,
		&u.BanExpire,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("user not found")
		} else {
			return nil, err
		}
	}

	return &u, nil
}
func (m *Model) GetByUsernameWithPassword(username string) (*User, error) {
	var u User
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		id,
		created_at,
		deleted_at,
		username,
		password,
		last_login,
		last_password_change,
		ban_date,
		ban_expire 
	FROM users
	WHERE username = $1`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Execute the query
	err = stmt.QueryRow(strings.ToLower(username)).Scan(
		&u.ID,
		&u.CreatedAt,
		&u.DeletedAt,
		&u.Username,
		&u.Password,
		&u.LastLogin,
		&u.LastPasswordChange,
		&u.BanDate,
		&u.BanExpire,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("user not found")
		} else {
			return nil, err
		}
	}

	return &u, nil
}
func (m *Model) HasPermission(id uuid.UUID, p *permission.Permission) (bool, error) {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		permissions.id as permission_id
	FROM users
	WHERE users.id = $1
		AND permissions.resource = $2
		AND permissions.operation = $3
	LEFT JOIN users_roles ON users.id = users_roles.user_id 
	LEFT JOIN roles ON users_roles.role_id = roles.id
	LEFT JOIN roles_permissions ON roles.id = roles_permissions.role_id
	LEFT JOIN permissions ON roles_permissions.permission_id = permissions.id
	`)
	if err != nil {
		return false, err
	}
	defer stmt.Close()

	// Execute the query
	var permissionID int
	err = stmt.QueryRow(id, p.Resource, p.Operation).Scan(
		&permissionID,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		} else {
			return false, err
		}
	}

	return true, nil
}
func (m *Model) HasVerifiedEmail(id uuid.UUID) (bool, error) {
	var verifiedEmail bool
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		verified_email 
	FROM users
	WHERE id = $1`)
	if err != nil {
		return false, err
	}
	defer stmt.Close()

	// Execute the query
	err = stmt.QueryRow(id).Scan(&verifiedEmail)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, errors.New("user not found")
		} else {
			return false, err
		}
	}

	return verifiedEmail, nil
}
func (m *Model) HasVerifiedPhone(id uuid.UUID) (bool, error) {
	var verifiedPhone bool
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		verified_phone 
	FROM users
	WHERE id = $1`)
	if err != nil {
		return false, err
	}
	defer stmt.Close()

	// Execute the query
	err = stmt.QueryRow(id).Scan(&verifiedPhone)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, errors.New("user not found")
		} else {
			return false, err
		}
	}

	return verifiedPhone, nil
}
func (m *Model) GetPhotoID(id uuid.UUID) (int, error) {
	var photoID *int
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		photo_id 
	FROM users
	WHERE id = $1`)
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	// Execute the query
	err = stmt.QueryRow(id).Scan(&photoID)
	if err != nil {
		if err == sql.ErrNoRows {
			return -1, errors.New("user not found")
		} else {
			return -1, err
		}
	}

	if photoID == nil {
		return -1, errors.New("user hasn't photo")
	}
	return *photoID, nil
}
func (m *Model) GetCVID(id uuid.UUID) (int, error) {
	var cvID *int
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	SELECT 
		cv_id 
	FROM users
	WHERE id = $1`)
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	// Execute the query
	err = stmt.QueryRow(id).Scan(&cvID)
	if err != nil {
		if err == sql.ErrNoRows {
			return -1, errors.New("user not found")
		} else {
			return -1, err
		}
	}

	if cvID == nil {
		return -1, errors.New("user hasn't CV")
	}
	return *cvID, nil
}
func (m *Model) Insert(u *User) error {
	// Begin transaction
	tx, err := m.DB.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	// Create the columns and values strings
	columns := []string{
		"username",
		"email",
		"password",
		"nick",
	}
	nick := u.Nick
	if u.Nick == "" {
		nick = u.Username
	}
	values := []string{
		"'" + strings.ToLower(u.Username) + "'",
		"'" + strings.ToLower(u.Email) + "'",
		"'" + u.Password + "'",
		"'" + nick + "'",
	}

	// Add additional columns and values if necessary
	if u.FirstName != nil {
		columns = append(columns, "first_name")
		values = append(values, "'"+*u.FirstName+"'")
	}
	if u.LastName != nil {
		columns = append(columns, "last_name")
		values = append(values, "'"+*u.LastName+"'")
	}
	if u.Phone != nil {
		columns = append(columns, "phone")
		values = append(values, "'"+*u.Phone+"'")
	}
	if u.Address != nil {
		columns = append(columns, "address")
		values = append(values, "'"+*u.Address+"'")
	}

	// Insert the user
	var id uuid.UUID
	err = tx.QueryRow("INSERT INTO users (" +
		strings.Join(columns, ", ") +
		") VALUES (" +
		strings.Join(values, ", ") +
		") RETURNING id;").Scan(&id)
	if err != nil {
		return err
	}

	// Check first user created
	var count int
	row := tx.QueryRow("SELECT COUNT(*) FROM users")
	err = row.Scan(&count)
	if err != nil {
		return err
	}
	if count == 1 {
		// get superadmin role id
		var superadminRoleID int
		row = tx.QueryRow("SELECT id FROM roles WHERE name = 'superadmin';")
		err = row.Scan(&superadminRoleID)
		if err != nil {
			return err
		}

		// Assign role to user (insert users_roles)
		_, err = tx.Exec("INSERT INTO users_roles (user_id, role_id) VALUES ($1, $2);", id, superadminRoleID)
		if err != nil {
			return err
		}
	}
	// Commit
	return tx.Commit()
}
func (m *Model) InsertMany(users []*User) error {
	// Create the columns and values strings
	columns := []string{
		"username",
		"email",
		"password",
		"nick",
		"first_name",
		"last_name",
		"phone",
		"address",
	}
	valuesLists := make([]string, len(users))
	for i, u := range users {
		// Set nick
		nick := u.Nick
		if u.Nick == "" {
			nick = u.Username
		}
		firstName := ""
		if u.FirstName != nil {
			firstName = *u.FirstName
		}
		lastName := ""
		if u.LastName != nil {
			lastName = *u.LastName
		}
		phone := ""
		if u.Phone != nil {
			phone = *u.Phone
		}
		address := ""
		if u.Address != nil {
			address = *u.Address
		}
		values := []string{
			"'" + strings.ToLower(u.Username) + "'",
			"'" + strings.ToLower(u.Email) + "'",
			"'" + fmt.Sprint(int(rand.Int63n(999999-100000+1))+100000) + "'", // random password, should call forget-password in order to log in
			"'" + nick + "'",
			"'" + firstName + "'",
			"'" + lastName + "'",
			"'" + phone + "'",
			"'" + address + "'",
		}
		if u.FirstName != nil {
			columns = append(columns, "first_name")
			values = append(values, *u.FirstName)
		}

		valuesLists[i] = "(" + strings.Join(values, ", ") + ")"
	}

	// Insert the users
	_, err := m.DB.Exec("INSERT INTO users (" + strings.Join(columns, ", ") + ") VALUES " + strings.Join(valuesLists, ", ") + ";")
	if err != nil {
		return err
	}

	return nil
}
func (m *Model) Update(u *User) error {
	// Prepare statement and values
	query := "UPDATE users SET "
	var cols []string
	var args []interface{}
	i := 1
	if u.Nick != "" {
		cols = append(cols, "nick = $"+fmt.Sprint(i))
		args = append(args, u.Nick)
		i++
	}
	if u.FirstName != nil {
		cols = append(cols, "first_name = $"+fmt.Sprint(i))
		args = append(args, *u.FirstName)
		i++
	}
	if u.LastName != nil {
		cols = append(cols, "last_name = $"+fmt.Sprint(i))
		args = append(args, *u.LastName)
		i++
	}
	if u.Phone != nil {
		cols = append(cols, "phone = $"+fmt.Sprint(i))
		args = append(args, *u.Phone)
		i++
	}
	if u.Address != nil {
		cols = append(cols, "address = $"+fmt.Sprint(i))
		args = append(args, *u.Address)
		i++
	}
	if len(cols) > 0 {
		cols = append(cols, "updated_at = NOW()")
	}
	query += strings.Join(cols, ", ") + " WHERE id = $" + fmt.Sprint(i)
	args = append(args, u.ID)

	stmt, err := m.DB.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(args...)

	return err
}
func (m *Model) UpdatePassword(id uuid.UUID, password string) error {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	UPDATE users SET 
		password = $1,
		last_password_change = NOW()
	WHERE id = $2`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(password, id)

	return err
}
func (m *Model) UpdateRoles(id uuid.UUID, roleIDs ...int) error {
	if len(roleIDs) == 0 {
		return errors.New("no role id parameter was given")
	}

	// Begin transaction
	tx, err := m.DB.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	// Clear user roles
	_, err = tx.Exec("DELETE from users_roles WHERE user_id = '" + id.String() + "';")
	if err != nil {
		return err
	}

	// Assign roles to user (insert users_roles)
	values := ""
	for _, roleID := range roleIDs {
		values += "('" + id.String() + "', " + fmt.Sprint(roleID) + "),"
	}
	values = strings.TrimSuffix(values, ",")

	_, err = tx.Exec("INSERT INTO users_roles (user_id, role_id) VALUES " + values + ";")
	if err != nil {
		return err
	}

	// Commit
	return tx.Commit()
}
func (m *Model) UpdatePhoto(id uuid.UUID, photoID int) error {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	UPDATE users SET 
		photo_id = $1,
	WHERE id = $2`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(photoID, id)

	return err
}
func (m *Model) UpdateCV(id uuid.UUID, cvID int) error {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	UPDATE users SET 
		cv_id = $1,
	WHERE id = $2`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(cvID, id)

	return err
}
func (m *Model) DeletePhoto(id uuid.UUID) error {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	UPDATE users SET 
		photo_id = NULL,
	WHERE id = $1`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(id)

	return err
}
func (m *Model) DeleteCV(id uuid.UUID) error {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	UPDATE users SET 
		cv_id = NULL,
	WHERE id = $1`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(id)

	return err
}
func (m *Model) VerifyEmail(id uuid.UUID) error {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	UPDATE users SET 
		verified_email = TRUE,
	WHERE id = $1`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(id)

	return err
}
func (m *Model) VerifyPhone(id uuid.UUID) error {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	UPDATE users SET 
		verified_phone = TRUE,
	WHERE id = $1`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(id)

	return err
}
func (m *Model) Ban(id uuid.UUID, banExpire time.Time) error {
	sqlStatement := `
		UPDATE users
		SET ban_date = NOW(), ban_expire = $2
		WHERE id = $1;`

	_, err := m.DB.Exec(sqlStatement, id.String(), banExpire)

	return err
}
func (m *Model) Unban(id uuid.UUID) error {
	sqlStatement := `
		UPDATE users
		SET ban_date = NULL, ban_expire = NULL
		WHERE id = $1;`

	_, err := m.DB.Exec(sqlStatement, id.String())

	return err
}
func (m *Model) Restore(id uuid.UUID) error {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	UPDATE users SET 
		deleted_at = NULL,
	WHERE id = $1`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(id)

	return err
}
func (m *Model) Delete(id uuid.UUID) error {
	// Prepare statement
	stmt, err := m.DB.Prepare(`
	UPDATE users SET 
		deleted_at = NOW(),
	WHERE id = $1`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Execute query
	_, err = stmt.Exec(id)

	return err
}
