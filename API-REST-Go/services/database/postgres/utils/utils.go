package utils

import (
	"database/sql"
	"errors"
)

func CheckIfTableExists(db *sql.DB, schemaName string, tableName string) (bool, error) {
	query := "SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = $1 AND table_name = $2)"
	row := db.QueryRow(query, schemaName, tableName)
	var exists bool
	err := row.Scan(&exists)
	if err != nil {
		return false, err
	}
	return exists, nil
}

func GetLimitAndOffset(page *int, pageSize *int) (limit int, offset int, err error) {
	limit = 100 // default 100 records max
	offset = 0
	if page != nil && pageSize != nil {
		if *page == 0 || *pageSize == 0 {
			err = errors.New("page and pageSize params must be greater than 0")
			return
		}
		limit = *pageSize
		offset = (*page - 1) * *pageSize
	}
	return
}
