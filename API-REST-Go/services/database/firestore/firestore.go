package firestore

import (
	"API-REST/services/database/firestore/models"
	"context"
	"os"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/option"
)

func Setup() error {
	// Read conf
	projectID := os.Getenv("GCP_PROJECT_ID")

	// Connect DB
	ctx := context.Background()
	client, err := firestore.NewClient(ctx, projectID, option.WithCredentialsJSON([]byte(os.Getenv("GCP_CREDENTIALS"))))
	if err != nil {
		return err
	}

	// Build Models
	models.Build(client)

	return nil
}

func Init() error {
	// Insert conf data
	// TODO
	return nil
}
