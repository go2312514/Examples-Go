package organization

import (
	"github.com/jschoedt/go-firestorm"
)

type Model struct {
	Coll *firestorm.FSClient
}

func New(coll *firestorm.FSClient) (*Model, error) {
	return &Model{Coll: coll}, nil
}
