package organization

import "fmt"

type OrganizationType int

const (
	Company = iota + 1
	Department
)

func (t OrganizationType) String() string {
	return [...]string{"Company", "Department"}[t-1]
}
func GetOrganizationType(orgType string) (OrganizationType, error) {
	if orgType == "Company" {
		return Company, nil
	} else if orgType == "Department" {
		return Department, nil
	}

	return -1, fmt.Errorf("incorrect organization type")
}
