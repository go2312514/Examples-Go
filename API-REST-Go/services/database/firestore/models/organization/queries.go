package organization

import (
	"context"
	"fmt"
	"strings"

	"cloud.google.com/go/firestore"
)

func (m *Model) GetAll(ctx context.Context, filterOptions map[string]interface{}) ([]*Organization, error) {
	organizations := make([]*Organization, 0)
	// Set filter options
	query := m.Coll.Client.Collection("organizations").Query
	for k, v := range filterOptions {
		query = query.Where(k, "==", v)
	}

	// Query data
	query = query.OrderBy("id", firestore.Asc)
	err := m.Coll.NewRequest().QueryEntities(ctx, query, &organizations)()
	if err != nil {
		return nil, err
	}

	return organizations, nil
}
func (m *Model) Get(ctx context.Context, id string) (*Organization, error) {
	organization := Organization{ID: id}
	_, err := m.Coll.NewRequest().GetEntities(ctx, &organization)()
	if err != nil {
		return nil, err
	}

	return &organization, nil
}
func (m *Model) Insert(ctx context.Context, organization *Organization) (string, error) {
	err := m.Coll.NewRequest().CreateEntities(ctx, &organization)()
	if err != nil {
		return "", formatErr(err)
	}
	return organization.ID, nil
}
func (m *Model) Update(ctx context.Context, organization *Organization) (string, error) {
	err := m.Coll.NewRequest().UpdateEntities(ctx, &organization)()
	if err != nil {
		return "", formatErr(err)
	}
	return organization.ID, nil
}
func (m *Model) Delete(ctx context.Context, id string) error {
	err := m.Coll.NewRequest().DeleteEntities(ctx, &Organization{ID: id})()
	return formatErr(err)
}

// Remove sensitive information from err
func formatErr(err error) error {
	errStr := fmt.Sprint(err)
	if strings.Contains(errStr, "not found") {
		err = fmt.Errorf("document not found")
	} else if strings.Contains(errStr, "already exist") {
		err = fmt.Errorf("document already exists")
	}
	return err
}
