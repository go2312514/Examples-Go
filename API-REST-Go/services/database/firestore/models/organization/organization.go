package organization

type Organization struct {
	ID       string           `json:"id"`
	Name     string           `json:"name" firestore:"name"`
	Type     OrganizationType `json:"type" firestore:"type"`
	Disabled bool             `json:"disabled,omitempty" firestore:"disabled"`
	// ...
}
