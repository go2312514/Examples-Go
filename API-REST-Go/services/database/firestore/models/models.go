package models

import (
	"API-REST/services/database/firestore/models/organization"

	"cloud.google.com/go/firestore"
	"github.com/jschoedt/go-firestorm"
)

var (
	Organization *organization.Model
	// ...
)

func Build(client *firestore.Client) error {
	var err error

	Organization, err = organization.New(firestorm.New(client, "organizations", ""))
	if err != nil {
		return err
	}
	// ...

	return nil
}
