package permission

type Permission struct {
	Resource  string `json:"resource,omitempty" mapstructure:"resource"`
	Operation string `json:"operation,omitempty" mapstructure:"operation"`
	// ...
}
