package permission

import (
	"context"
	"strings"
)

func (m *Model) GetRolePermissions(role string) ([]*Permission, error) {
	var permissions []*Permission
	// Execute the query
	getAll := m.Client.SMembers(context.TODO(), "role:"+role)
	if getAll.Err() != nil {
		return nil, getAll.Err()
	}

	for _, p := range getAll.Val() {
		pSplit := strings.Split(p, ":")
		resource := pSplit[0]
		operation := pSplit[1]
		permissions = append(permissions, &Permission{
			Resource:  resource,
			Operation: operation,
		})
	}

	return permissions, nil
}

func (m *Model) SetRolePermissions(role string, permissions []*Permission) error {
	var values []string
	for _, p := range permissions {
		values = append(values, p.Resource+":"+p.Operation)
	}

	// Execute the query
	err := m.Client.SAdd(context.TODO(), "role:"+role, values).Err()
	if err != nil {
		return err
	}

	return nil
}
