package session

import (
	"github.com/redis/go-redis/v9"
)

type Model struct {
	Client *redis.Client
}

func New(client *redis.Client) (*Model, error) {
	return &Model{Client: client}, nil
}
