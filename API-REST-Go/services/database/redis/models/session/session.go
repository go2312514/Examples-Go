package session

import (
	"github.com/google/uuid"
)

type UserInfo struct {
	UserID uuid.UUID `json:"id" mapstructure:"id"`

	Username string `json:"username" mapstructure:"username"`
	Email    string `json:"email" mapstructure:"email"`

	Nick      string `json:"nick" mapstructure:"nick"`
	FirstName string `json:"first_name,omitempty" mapstructure:"first_name"`
	LastName  string `json:"last_name,omitempty" mapstructure:"last_name"`
	Phone     string `json:"phone,omitempty" mapstructure:"phone"`
	Address   string `json:"address,omitempty" mapstructure:"address"`
}

type UserRoles struct {
	Roles []string `json:"roles,omitempty"`
}
