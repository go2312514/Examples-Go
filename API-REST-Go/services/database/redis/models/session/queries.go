package session

import (
	"API-REST/api-gateway/utilities"
	"context"

	"github.com/google/uuid"
	"github.com/mitchellh/mapstructure"
)

func (m *Model) GetUserInfo(userID uuid.UUID) (*UserInfo, error) {
	var info UserInfo

	// Execute the query
	getAll := m.Client.HGetAll(context.TODO(), "user:"+userID.String()+":info")
	if getAll.Err() != nil {
		return nil, getAll.Err()
	}

	infoMap := getAll.Val()

	mapstructure.Decode(infoMap, &info)

	return &info, nil
}

func (m *Model) SetUserInfo(info *UserInfo) error {
	infoMap := utilities.StructToMap(info)

	// Execute the query
	for k, v := range infoMap {
		err := m.Client.HSet(context.TODO(), "user:"+info.UserID.String()+":info", k, v).Err()
		if err != nil {
			return err
		}
	}

	return nil
}

func (m *Model) GetUserRoles(userID uuid.UUID) ([]string, error) {
	// Execute the query
	getAll := m.Client.SMembers(context.TODO(), "user:"+userID.String()+":roles")
	if getAll.Err() != nil {
		return nil, getAll.Err()
	}

	return getAll.Val(), nil
}

func (m *Model) SetUserRoles(userID uuid.UUID, roles []string) error {
	// Execute the query
	err := m.Client.SAdd(context.TODO(), "user:"+userID.String()+":roles", roles).Err()
	if err != nil {
		return err
	}

	return nil
}
