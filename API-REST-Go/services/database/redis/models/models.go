package models

import (
	"API-REST/services/database/redis/models/permission"
	"API-REST/services/database/redis/models/session"

	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
)

var (
	Permission *permission.Model
	Session    *session.Model
	// ...
)

func Build(client *redis.Client) error {
	var err error

	Permission, err = permission.New(client)
	if err != nil {
		return err
	}
	Session, err = session.New(client)
	if err != nil {
		return err
	}
	// ...

	// Borrar
	Permission.SetRolePermissions("admin", []*permission.Permission{
		&permission.Permission{Resource: "users", Operation: "GET"},
		&permission.Permission{Resource: "users", Operation: "POST"},
		&permission.Permission{Resource: "roles", Operation: "PUT"},
	})
	Session.SetUserInfo(&session.UserInfo{
		UserID:    uuid.Max,
		Username:  "username",
		Email:     "email@gmail.com",
		Nick:      "nick",
		FirstName: "first",
		LastName:  "last",
		Phone:     "633360930",
		Address:   "calle mis santos",
	})
	Session.SetUserRoles(uuid.Max, []string{"user", "admin"})

	return nil
}
