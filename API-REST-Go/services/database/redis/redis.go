package redis

import (
	"API-REST/services/conf"
	"API-REST/services/database/redis/models"
	"context"

	"github.com/redis/go-redis/v9"
)

func Setup() error {
	// Read conf
	host := conf.Env.GetString("REDIS_HOST")
	port := conf.Env.GetString("REDIS_PORT")

	// Connect DB
	client := redis.NewClient(&redis.Options{
		Addr:     host + ":" + port,
		Username: "", // no user set
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	s := client.Ping(context.Background())
	if s.Err() != nil {
		return s.Err()
	}

	// Build Models
	models.Build(client)

	return nil
}

func Init() error {
	// Insert conf data
	// TODO
	return nil
}
