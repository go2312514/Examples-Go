package database

import (
	"API-REST/services/database/firestore"
	"API-REST/services/database/mongo"
	"API-REST/services/database/postgres"
	"API-REST/services/database/redis"
)

func SetupPostgres() error {
	return postgres.Setup()
}
func SetupMongo() error {
	return mongo.Setup()
}
func SetupFirestore() error {
	return firestore.Setup()
}
func SetupRedis() error {
	return redis.Setup()
}

func SetupPostgresDockertest() error {
	return postgres.SetupDockertest()
}

// Create DB and tables
func Init() error {
	return postgres.Init()
}
