package attribute

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (m *Model) GetAll(fromDate time.Time, toDate time.Time, filterOptions map[string]interface{}) ([]*Attribute, error) {
	atts := make([]*Attribute, 0)
	// Set filter options
	filter := bson.D{}
	filterDate := bson.D{}

	if !fromDate.IsZero() {
		filterDate = append(filterDate, bson.E{"$gt", fromDate})
		filter = bson.D{{"timestamp", filterDate}}
	}
	if !toDate.IsZero() {
		filterDate = append(filterDate, bson.E{"$lt", toDate})
		filter = bson.D{{"timestamp", filterDate}}
	}

	if len(filterOptions) != 0 {
		if name, ok := filterOptions["name"]; ok {
			filter = append(filter, bson.E{"metadata.name", name})
		}
		if label, ok := filterOptions["label"]; ok {
			filter = append(filter, bson.E{"metadata.label", label})
		}
		// other options ...
	}

	// Query data
	cur, err := m.Coll.Find(context.TODO(),
		filter,
	)
	if err != nil {
		return nil, err
	}

	for cur.Next(context.TODO()) {
		//Create a value into which the single document can be decoded
		var att Attribute
		err := cur.Decode(&att)
		if err != nil {
			return nil, err
		}

		atts = append(atts, &att)
	}
	//Close the cursor once finished
	cur.Close(context.TODO())

	return atts, nil
}
func (m *Model) Get(id string) (*Attribute, error) {
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	var att Attribute
	r := m.Coll.FindOne(context.TODO(),
		bson.M{
			"_id": objID,
		},
	)
	err = r.Decode(&att)
	if err != nil {
		return nil, err
	}
	return &att, nil
}
func (m *Model) Insert(attribute *Attribute) (primitive.ObjectID, error) {
	res, err := m.Coll.InsertOne(context.TODO(),
		bson.D{
			{"metadata", bson.D{
				{"asset_name", attribute.Metadata.AssetName},
				{"name", attribute.Metadata.Name},
				{"label", attribute.Metadata.Label},
				{"unit", attribute.Metadata.Unit},
			}},
			{"timestamp", attribute.Timestamp},
			{"value", attribute.Value},
		},
	)

	return res.InsertedID.(primitive.ObjectID), err
}
func (m *Model) InsertMany(attributes []*Attribute) ([]primitive.ObjectID, error) {
	var documents []interface{}
	for _, att := range attributes {
		documents = append(documents,
			bson.D{
				{"metadata", bson.D{
					{"asset_name", att.Metadata.AssetName},
					{"name", att.Metadata.Name},
					{"label", att.Metadata.Label},
					{"unit", att.Metadata.Unit},
				}},
				{"timestamp", att.Timestamp},
				{"value", att.Value},
			})
	}
	res, err := m.Coll.InsertMany(context.TODO(), documents)
	var ids []primitive.ObjectID
	for _, id := range res.InsertedIDs {
		ids = append(ids, id.(primitive.ObjectID))
	}
	return ids, err
}
func (m *Model) Update(attribute *Attribute) error {
	var keyValues []primitive.E
	if attribute.Metadata.AssetName != "" {
		keyValues = append(keyValues, primitive.E{"metadata.asset_name", attribute.Metadata.AssetName})
	}
	if attribute.Metadata.Name != "" {
		keyValues = append(keyValues, primitive.E{"metadata.name", attribute.Metadata.Name})
	}
	if attribute.Metadata.Label != "" {
		keyValues = append(keyValues, primitive.E{"metadata.label", attribute.Metadata.Label})
	}
	if attribute.Metadata.Unit != "" {
		keyValues = append(keyValues, primitive.E{"metadata.unit", attribute.Metadata.Unit})
	}
	if attribute.Timestamp != nil && attribute.Timestamp.IsZero() {
		keyValues = append(keyValues, primitive.E{"timestamp", *attribute.Timestamp})
	}
	if attribute.Value != nil {
		keyValues = append(keyValues, primitive.E{"value", *attribute.Value})
	}

	_, err := m.Coll.UpdateOne(
		context.TODO(),
		bson.D{
			{"_id", attribute.ID},
		},
		bson.D{
			{"$set", keyValues},
		},
	)

	return err
}
func (m *Model) Delete(id string) error {
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}
	_, err = m.Coll.DeleteOne(
		context.TODO(),
		bson.D{
			{"_id", objID},
		},
	)

	return err
}
