package marshaller

import (
	"API-REST/services/database/postgres/models/user"

	"github.com/gocarina/gocsv"
)

func InterfaceToCSV(in interface{}) ([]byte, error) {
	return gocsv.MarshalBytes(in)
}

func CSVToUsers(in []byte) ([]*user.User, error) {
	out := []*user.User{}
	err := gocsv.UnmarshalBytes(in, &out)
	return out, err
}
