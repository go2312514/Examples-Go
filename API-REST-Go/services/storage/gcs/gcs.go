package gcs

import (
	"API-REST/services/conf"
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"cloud.google.com/go/storage"
	"google.golang.org/api/googleapi"
	"google.golang.org/api/option"
)

type Storage struct {
	client  *storage.Client
	bucket  *storage.BucketHandle
	maxSize int
}

func Setup() (*Storage, error) {
	var err error

	s := &Storage{
		maxSize: conf.Conf.GetInt("storage.gcs.maxSize"),
	}
	opt := option.WithCredentialsJSON([]byte(os.Getenv("GCP_CREDENTIALS")))
	s.client, err = storage.NewClient(context.Background(), opt)
	if err != nil {
		return nil, err
	}
	s.bucket = s.client.Bucket(conf.Conf.GetString("storage.gcs.bucket"))

	return s, nil
}

func (s *Storage) Name() string {
	return "gcs"
}

func (s *Storage) SaveFile(b []byte, filename string, subFolders ...string) error {
	// Check file size
	size := len(b)
	if size > s.maxSize {
		return fmt.Errorf("file size too big: %d bytes (max=%d)", size, s.maxSize)
	}

	// Construct object name with prefix from folderPath (create subbuckets or subfolders in gcs is not posible)
	if len(subFolders) > 0 && len(subFolders[0]) > 0 {
		filename = path.Join(subFolders...) + "/" + filename
	}

	// Upload file
	w := s.bucket.Object(filename).NewWriter(context.TODO())
	_, err := w.Write(b)
	if err != nil {
		return err
	}
	return w.Close()
}

func (s *Storage) GetFile(filename string, subFolders ...string) ([]byte, error) {
	// Construct object name with prefix from folderPath (create subbuckets or subfolders in gcs is not posible)
	if len(subFolders) > 0 && len(subFolders[0]) > 0 {
		filename = path.Join(subFolders...) + "/" + filename
	}

	// Get file from gcs
	r, err := s.bucket.Object(filename).NewReader(context.TODO())
	if err != nil {
		return nil, err
	}
	// Get file content and return it
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func (s *Storage) DeleteFile(filename string, subFolders ...string) error {
	// Construct object name with prefix from folderPath (create subbuckets or subfolders in gcs is not posible)
	if len(subFolders) > 0 && len(subFolders[0]) > 0 {
		filename = path.Join(subFolders...) + "/" + filename
	}

	// Delete file
	err := s.bucket.Object(filename).Delete(context.TODO())
	if err != nil {
		if gErr, ok := err.(*googleapi.Error); ok && gErr.Code == 404 {
			return errors.New("not found")
		}
	}
	return err
}
