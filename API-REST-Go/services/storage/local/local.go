package local

import (
	"API-REST/services/conf"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
)

type Storage struct {
	storageDir string
	maxSize    int
}

func Setup() (*Storage, error) {
	s := Storage{
		storageDir: conf.Conf.GetString("storage.local.rootDir"),
		maxSize:    conf.Conf.GetInt("storage.local.maxSize"),
	}
	err := os.Mkdir(s.storageDir, os.ModePerm)
	if err != nil && !os.IsExist(err) {
		return nil, err
	}

	return &s, nil
}

func (s *Storage) Name() string {
	return "local"
}

func (s *Storage) SaveFile(b []byte, filename string, subFolders ...string) error {

	// Check file size
	size := len(b)
	if size > s.maxSize {
		return fmt.Errorf("file size too big: %d bytes (max=%d)", size, s.maxSize)
	}

	// Set filepath and create folders recursively if not exist
	filePath := s.storageDir + "/" + filename
	if len(subFolders) > 0 && len(subFolders[0]) > 0 {
		folderPath := path.Join(subFolders...)
		err := os.MkdirAll(s.storageDir+"/"+folderPath, os.ModePerm)
		if err != nil {
			return err
		}
		filePath = s.storageDir + "/" + folderPath + "/" + filename
	}
	// open output file
	out, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer out.Close()

	// write content into output file
	_, err = out.Write(b)
	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) GetFile(filename string, subFolders ...string) ([]byte, error) {
	// Set filepath
	filePath := s.storageDir + "/" + filename
	if len(subFolders) > 0 && len(subFolders[0]) > 0 {
		folderPath := path.Join(subFolders...)
		filePath = s.storageDir + "/" + folderPath + "/" + filename
	}
	// Read file
	b, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func (s *Storage) DeleteFile(filename string, subFolders ...string) error {
	// Set filepath
	filePath := s.storageDir + "/" + filename
	if len(subFolders) > 0 && len(subFolders[0]) > 0 {
		folderPath := path.Join(subFolders...)
		filePath = s.storageDir + "/" + folderPath + "/" + filename
	}
	// Delete file
	err := os.Remove(filePath)
	if err != nil && os.IsNotExist(err) {
		return errors.New("not found")
	}
	return err
}
