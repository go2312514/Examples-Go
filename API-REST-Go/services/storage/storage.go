package storage

import (
	"API-REST/services/storage/gcs"
	"API-REST/services/storage/local"
)

type Storage interface {
	Name() string
	SaveFile(b []byte, filename string, subFolders ...string) error
	GetFile(filename string, subFolders ...string) ([]byte, error)
	DeleteFile(filename string, subFolders ...string) error
}

var Local *local.Storage
var GCS *gcs.Storage

func SetupLocal() error {
	s, err := local.Setup()
	Local = s
	return err
}
func SetupGCS() error {
	s, err := gcs.Setup()
	GCS = s
	return err
}
