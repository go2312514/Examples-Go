/*
Copyright © 2022 Demetrio Navarro Martínez <deme1994@gmail.com>
*/
package cmd

import (
	"API-REST/api-gateway"
	"API-REST/api-gateway/controllers"
	"API-REST/services/conf"
	"API-REST/services/database"
	"API-REST/services/logger"
	"API-REST/services/storage"
	"log"

	"github.com/spf13/cobra"
)

// cleanCmd represents the clean command
var cleanCmd = &cobra.Command{
	Use:   "clean",
	Short: "Clean orphaned data",
	Long: `Clean data using api controllers or services to make sure any orphaned data is removed
	from database and/or storage. For example, users that have signed up but they never confirmed their email
	in a long time (if required), or stored files which can't be fetched or have lost their reference to other table.
	This command is supposed to be executed in a cron job.`,
	Run: func(cmd *cobra.Command, args []string) {
		log.SetFlags(log.LstdFlags | log.Lshortfile) // Set default log flags (print file and line)

		// Conf
		log.Println("Loading configuration service...")
		err := conf.Setup("", "")
		if err != nil {
			log.Fatal("\033[31m"+"CONFIGURATION SERVICE FAILED"+"\033[0m"+" -> ", err)
		}
		log.Println("\033[32m" + "CONFIGURATION SERVICE IS RUNNING" + "\033[0m")

		// Logger
		log.Println("Loading logging service...")
		err = logger.Setup()
		if err != nil {
			log.Fatal("\033[31m"+"LOGGING SERVICE FAILED"+"\033[0m"+" -> ", err)
		}
		log.Println("\033[32m" + "LOGGING SERVICE IS RUNNING" + "\033[0m")

		// Storage
		log.Println("Loading storage service...")
		err = storage.SetupLocal()
		if err != nil {
			log.Fatal("\033[31m"+"STORAGE SERVICE FAILED"+"\033[0m"+" -> ", err)
		}
		err = storage.SetupGCS()
		if err != nil {
			log.Fatal("\033[31m"+"GC STORAGE SERVICE FAILED"+"\033[0m"+" -> ", err)
		}
		log.Println("\033[32m" + "STORAGE SERVICE IS RUNNING" + "\033[0m")

		// DB
		log.Println("Loading database service...")
		err = database.SetupPostgres()
		if err != nil {
			log.Fatal("\033[31m"+"DATABASE SERVICE FAILED"+"\033[0m"+" -> ", err)
		}
		err = database.SetupMongo()
		if err != nil {
			log.Fatal("\033[31m"+"DATABASE SERVICE FAILED"+"\033[0m"+" -> ", err)
		}
		log.Println("\033[32m" + "DATABASE SERVICE IS RUNNING" + "\033[0m")

		// API-Gateway
		log.Println("Loading api-gateway controllers...")
		api.Setup()

		// Clean db and/or storage
		err = clean()
		if err != nil {
			log.Fatal("\033[31m"+"CLEAN ERROR"+"\033[0m"+" -> ", err)
		}
		log.Println("\033[32m" + "DATABASE AND/OR STORAGE WERE SUCCESSFULLY CLEANED" + "\033[0m")

		logger.Logger.Println("DATABASE AND/OR STORAGE HAVE BEEN CLEANED")
	},
}

func init() {
	rootCmd.AddCommand(cleanCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// cleanCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// cleanCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func clean() error {
	// Remove orphaned data via api controllers
	err := controllers.PurgeOrphanedData()
	if err != nil {
		return err
	}

	return nil
}
