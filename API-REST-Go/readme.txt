Comandos:
    go run main.go
    go run main.go --help
    go run main.go init --resetdb=true
    go run main.go clean

Instrucciones de despliegue:
    1. Crear el archivo application_default_credentials.json de google cloud para que funcione el storage: 
        https://cloud.google.com/docs/authentication/provide-credentials-adc?hl=es-419#local-dev


Consideraciones:
    - El primer user en hacer signup es el superadmin!!
    - Es recomendable crear una api separada para carga de archivos donde se establezca un BodyLimit superior, 
    esto es más para microservicios ya que el servicio de autorización debe ser otro distinto. 