package api

import (
	"API-REST/api-gateway/controllers"
	"API-REST/api-gateway/middleware"
	util "API-REST/api-gateway/utilities"
	"API-REST/services/conf"
	"API-REST/services/logger"
	"API-REST/services/storage"
	"net/http"

	"github.com/goccy/go-json"
	"github.com/gofiber/fiber/v2"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/valyala/fasthttp/fasthttpadaptor"
)

var App *fiber.App

func Start() error {
	// Run server
	host := conf.Env.GetString("HOST")
	port := conf.Env.GetString("PORT")
	logger.Logger.Printf("Starting server on http://%s:%s\n", host, port)
	return App.Listen(conf.Env.GetString("HOST") + ":" + conf.Env.GetString("PORT"))
}

func Setup() {
	// Build controllers
	controllers.Build()

	// Setup Fiber api server
	App = fiber.New(fiber.Config{
		JSONEncoder: json.Marshal,
		JSONDecoder: json.Unmarshal,
		BodyLimit:   conf.Conf.GetInt("apiBodyMaxSize"),
	})

	// Setup groups and middleware
	api := App.Group(conf.Conf.GetString("apiBasePath"), middleware.CORS())
	pub := api.Group("/public")
	pvt := api.Group("/private", middleware.CheckToken)
	pvtVer := pvt.Group("/verified", middleware.VerifyEmail)

	gis := App.Group(conf.Conf.GetString("gisBasePath"), middleware.CORS())
	gis.Use(middleware.CORS(), middleware.CheckToken, middleware.VerifyEmail)

	// Api status path
	api.Get("/status", func(ctx *fiber.Ctx) error {
		appStatus := struct {
			Status      string `json:"status"`
			Environment string `json:"environment"`
			Version     string `json:"version"`
		}{
			Status:      "Available",
			Environment: conf.Env.GetString("ENVIRONMENT"),
			Version:     conf.Env.GetString("VERSION"),
		}

		return util.WriteJSON(ctx, http.StatusOK, appStatus, "status")
	})

	// // Exclusive path for serving all files (superadmin required)
	// App.Get("/",
	// 	middleware.CheckToken,
	// 	middleware.CheckPermission("*", "*"),
	// 	// Serve all files of project
	// 	filesystem.New(filesystem.Config{
	// 		Root:   http.Dir("./"),
	// 		Browse: true,
	// 	}),
	// )

	// This is the endpoint used by prometheus
	api.Get("/metrics", func(ctx *fiber.Ctx) error {
		// Convert the promhttp.Handler (http.Handler) to a fiber.Handler
		fastHttpHandler := fasthttpadaptor.NewFastHTTPHandler(promhttp.Handler())
		fastHttpHandler(ctx.Context())
		return nil
	})

	pub.Post("/auth/login", controllers.User.Auth.Login)
	pub.Post("/auth/confirm-email/:token", controllers.User.Auth.ConfirmEmail)
	pub.Post("/auth/forgot-password", controllers.User.Auth.ForgotPassword)
	pub.Post("/auth/reset-password", controllers.User.Auth.ResetPassword)
	pvt.Post("/auth/send-confirmation-email", controllers.User.Auth.SendConfirmationEmail)
	pvt.Post("/auth/send-confirmation-phone", controllers.User.Auth.SendConfirmationPhone)
	pvt.Post("/auth/confirm-phone/:code", controllers.User.Auth.ConfirmPhone)

	pvt.Get("/users/me", controllers.User.Auth.Get)
	pvt.Get("/users/me/photo", controllers.User.Auth.DownloadPhoto, controllers.File.Download)
	pvt.Get("/users/me/cv", controllers.User.Auth.DownloadCV, controllers.File.Download)
	pvt.Put("/users/me", controllers.User.Auth.Update)
	pvt.Put("/users/me/change-password", controllers.User.Auth.ChangePassword)
	pvt.Post("/users/me/photo", controllers.File.Insert(true, storage.Local, "users/photos", ".png", ".jpg", ".jpeg", ".webp"), controllers.User.Auth.UpdatePhoto)
	pvt.Post("/users/me/cv", controllers.File.Insert(true, storage.Local, "users/cvs", ".pdf"), controllers.User.Auth.UpdateCV)
	pvt.Delete("/users/me/photo", controllers.User.Auth.DeletePhoto, controllers.File.Delete)
	pvt.Delete("/users/me/cv", controllers.User.Auth.DeleteCV, controllers.File.Delete)
	pvt.Delete("/users/me", controllers.User.Auth.Delete)

	pvtVer.Get("/users", middleware.CheckPermission("users", "read"), controllers.User.GetAll)
	pvtVer.Get("/users/:id", middleware.CheckPermission("users", "read"), controllers.User.Get)
	pvtVer.Get("/users/:id/photo", middleware.CheckPermission("users", "read"), controllers.User.DownloadPhoto, controllers.File.Download)
	pvtVer.Get("/users/:id/cv", middleware.CheckPermission("users", "read"), controllers.User.DownloadCV, controllers.File.Download)
	pub.Post("/users", controllers.User.Insert) // public registration
	pvtVer.Post("/users/:id/roles", middleware.CheckPermission("users", "assign"), controllers.User.UpdateRoles)
	pvtVer.Put("/users/:id", middleware.CheckPermission("users", "update"), controllers.User.Update)
	pvtVer.Post("/users/:id/photo", middleware.CheckPermission("users", "update"), controllers.File.Insert(true, storage.Local, "users/photos", ".png", ".jpg", ".jpeg", ".webp"), controllers.User.UpdatePhoto)
	pvtVer.Post("/users/:id/cv", middleware.CheckPermission("users", "update"), controllers.File.Insert(true, storage.Local, "users/cvs", ".pdf"), controllers.User.UpdateCV)
	pvtVer.Delete("/users/:id/photo", middleware.CheckPermission("users", "update"), controllers.User.DeletePhoto, controllers.File.Delete)
	pvtVer.Delete("/users/:id/cv", middleware.CheckPermission("users", "update"), controllers.User.DeleteCV, controllers.File.Delete)
	pvtVer.Put("/users/:id/ban", middleware.CheckPermission("users", "ban"), controllers.User.Ban)
	pvtVer.Put("/users/:id/unban", middleware.CheckPermission("users", "ban"), controllers.User.Unban)
	pvtVer.Put("/users/:id/restore", middleware.CheckPermission("users", "delete"), controllers.User.Restore)
	pvtVer.Delete("/users/:id", middleware.CheckPermission("users", "delete"), controllers.User.Delete)
	pvtVer.Post("/import/users", middleware.CheckPermission("users", "import"), controllers.User.ImportCSV)
	pvtVer.Get("/export/users", middleware.CheckPermission("users", "read"), controllers.User.ExportCSV)

	pvtVer.Get("/roles", middleware.CheckPermission("roles", "read"), controllers.Role.GetAll)
	pvtVer.Get("/roles/:id", middleware.CheckPermission("roles", "read"), controllers.Role.Get)
	pvtVer.Post("/roles", middleware.CheckPermission("roles", "create"), controllers.Role.Insert)
	pvtVer.Post("/roles/:id/permissions", middleware.CheckPermission("roles", "assign"), controllers.Role.UpdatePermissions)
	pvtVer.Put("/roles/:id", middleware.CheckPermission("roles", "update"), controllers.Role.Update)
	pvtVer.Delete("/roles/:id", middleware.CheckPermission("roles", "delete"), controllers.Role.Delete)

	pvtVer.Get("/permissions", middleware.CheckPermission("permissions", "read"), controllers.Permission.GetAll)
	pvtVer.Get("/permissions/:id", middleware.CheckPermission("permissions", "read"), controllers.Permission.Get)
	//pvtVer.Post("/permissions", middleware.CheckPermission("permissions", "create"), controllers.Permission.Insert)
	//pvtVer.Put("/permissions/:id", middleware.CheckPermission("permissions", "update"), controllers.Permission.Update)
	//pvtVer.Delete("/permissions/:id", middleware.CheckPermission("permissions", "delete"), controllers.Permission.Delete)

	pvtVer.Get("/files", middleware.CheckPermission("files", "read"), controllers.File.GetAll)
	pvtVer.Get("/files/:id", middleware.CheckPermission("files", "read"), controllers.File.Get)
	pvtVer.Get("/files/:id/download", middleware.CheckPermission("files", "read"), controllers.File.Download)
	pvtVer.Get("/files-extensions", middleware.CheckPermission("files", "read"), controllers.File.GetExtensions)
	pvtVer.Post("/files", middleware.CheckPermission("files", "create"), controllers.File.Insert(false, storage.Local, ""))
	pvtVer.Delete("/files/:id", middleware.CheckPermission("files", "delete"), controllers.File.Delete)

	pvtVer.Get("/logs", middleware.CheckPermission("logs", "read"), controllers.Log.GetAll)
	pvtVer.Get("/logs/:filename", middleware.CheckPermission("logs", "read"), controllers.Log.Get)
	pvtVer.Delete("/logs/:filename", middleware.CheckPermission("logs", "delete"), controllers.Log.Delete)

	pvtVer.Post("/features", middleware.CheckPermission("features", "create"), controllers.Feature.Insert)
	pvtVer.Get("/features", middleware.CheckPermission("features", "read"), controllers.Feature.GetAll)
	gis.Get("/maplibre-martin/**", middleware.CheckPermission("features", "read"), controllers.Feature.GetVectorTiles)

	pvtVer.Get("/assets", middleware.CheckPermission("assets", "read"), controllers.Asset.GetAll)
	pvtVer.Get("/assets/:id", middleware.CheckPermission("assets", "read"), controllers.Asset.Get)
	pvtVer.Get("/assets/:id/attributes", middleware.CheckPermission("assets", "read"), controllers.Asset.GetWithAttributes)
	pvtVer.Get("/assets/names", middleware.CheckPermission("assets", "read"), controllers.Asset.GetNames)
	pvtVer.Post("/assets", middleware.CheckPermission("assets", "create"), controllers.Asset.Insert)
	pvtVer.Put("/assets/:id", middleware.CheckPermission("assets", "update"), controllers.Asset.Update)
	pvtVer.Delete("/assets/:id", middleware.CheckPermission("assets", "delete"), controllers.Asset.Delete)

	pvtVer.Get("/attributes", middleware.CheckPermission("attributes", "read"), controllers.Attribute.GetAll)
	pvtVer.Get("/attributes/:id", middleware.CheckPermission("attributes", "read"), controllers.Attribute.Get)
	pvtVer.Post("/attributes", middleware.CheckPermission("attributes", "create"), controllers.Attribute.Insert)
	pvtVer.Put("/attributes/:id", middleware.CheckPermission("attributes", "update"), controllers.Attribute.Update)
	pvtVer.Delete("/attributes/:id", middleware.CheckPermission("attributes", "delete"), controllers.Attribute.Delete)

	pvtVer.Get("/organizations", middleware.CheckPermission("organizations", "read"), controllers.Organization.GetAll)
	pvtVer.Get("/organizations/:id", middleware.CheckPermission("organizations", "read"), controllers.Organization.Get)
	pvtVer.Post("/organizations", middleware.CheckPermission("organizations", "create"), controllers.Organization.Insert)
	pvtVer.Put("/organizations/:id", middleware.CheckPermission("organizations", "update"), controllers.Organization.Update)
	pvtVer.Delete("/organizations/:id", middleware.CheckPermission("organizations", "delete"), controllers.Organization.Delete)
	// ...
}
