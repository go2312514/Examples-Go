package role

import (
	"API-REST/api-gateway/controllers/role/payloads"
	util "API-REST/api-gateway/utilities"
	"API-REST/services/database/postgres/models/role"
	"API-REST/services/logger"
	"API-REST/services/monitor"
	"errors"
	"net/http"
	"strconv"

	"github.com/gofiber/fiber/v2"
)

func (c *Controller) GetAll(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	// Query parameters
	var queryParams payloads.QueryParams
	err := ctx.QueryParser(&queryParams)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(queryParams)
	if err != nil {
		return err
	}

	roles, err := c.Model.GetAll(&role.QueryParams{
		Page:     queryParams.Page,
		PageSize: queryParams.PageSize,
		Name:     queryParams.Name,
	})
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := make([]*payloads.GetResponse, 0)
	for _, role := range roles {
		response = append(response, &payloads.GetResponse{
			ID:   role.ID,
			Name: role.Name,
		})
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Get(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := strconv.Atoi(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	role, err := c.Model.Get(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := payloads.GetResponse{
		ID:   role.ID,
		Name: role.Name,
	}
	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Insert(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	var req payloads.InsertRequest

	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	id, err := c.Model.Insert(&role.Role{Name: req.Name})
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := payloads.InsertResponse{ID: id}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Update(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := strconv.Atoi(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	var req payloads.UpdateRequest

	err = ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	var role role.Role
	role.ID = id
	role.Name = req.Name

	err = c.Model.Update(&role)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) UpdatePermissions(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := strconv.Atoi(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	var req payloads.UpdatePermissionsRequest

	err = ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	err = c.Model.UpdatePermissions(id, req.PermissionIDs...)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) Delete(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := strconv.Atoi(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	err = c.Model.Delete(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
