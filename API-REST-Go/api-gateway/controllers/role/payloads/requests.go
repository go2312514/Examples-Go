package payloads

type QueryParams struct {
	Page     *int    `query:"page" validate:"required_with=PageSize"`
	PageSize *int    `query:"pageSize" validate:"required_with=Page"`
	Name     *string `query:"name"`
}

type InsertRequest struct {
	Name string `json:"name" validate:"required"`
}

type UpdateRequest struct {
	Name string `json:"name" validate:"required"`
}

type UpdatePermissionsRequest struct {
	PermissionIDs []int `json:"permission_ids" validate:"required"`
}
