package payloads

type GetResponse struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type InsertResponse struct {
	ID int `json:"id"`
}
