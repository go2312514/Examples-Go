package payloads

type QueryParams struct {
	Page      *int    `query:"page"`
	PageSize  *int    `query:"pageSize"`
	Resource  *string `query:"resource"`
	Operation *string `query:"operation"`
}

type InsertRequest struct {
	Resource  string `json:"resource" validate:"required"`
	Operation string `json:"operation" validate:"required"`
}

type UpdateRequest struct {
	Resource  string `json:"resource" validate:"required"`
	Operation string `json:"operation" validate:"required"`
}
