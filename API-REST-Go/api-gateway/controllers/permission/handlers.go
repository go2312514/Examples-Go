package permission

import (
	"API-REST/api-gateway/controllers/permission/payloads"
	util "API-REST/api-gateway/utilities"
	"API-REST/services/database/postgres/models/permission"
	"API-REST/services/logger"
	"API-REST/services/monitor"
	"errors"
	"net/http"
	"strconv"

	"github.com/gofiber/fiber/v2"
)

func (c *Controller) GetAll(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	// Query parameters
	var queryParams payloads.QueryParams
	err := ctx.QueryParser(&queryParams)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(queryParams)
	if err != nil {
		return err
	}

	permissions, err := c.Model.GetAll(&permission.QueryParams{
		Page:      queryParams.Page,
		PageSize:  queryParams.PageSize,
		Resource:  queryParams.Resource,
		Operation: queryParams.Operation,
	})
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := make([]*payloads.GetResponse, 0)
	for _, permission := range permissions {
		response = append(response, &payloads.GetResponse{
			ID:        permission.ID,
			Resource:  permission.Resource,
			Operation: permission.Operation,
		})
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Get(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := strconv.Atoi(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	permission, err := c.Model.Get(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := payloads.GetResponse{
		ID:        permission.ID,
		Resource:  permission.Resource,
		Operation: permission.Operation,
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
