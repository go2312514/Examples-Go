package file

import (
	"API-REST/api-gateway/controllers/file/payloads"
	util "API-REST/api-gateway/utilities"
	"API-REST/services/database/postgres/models/file"
	"API-REST/services/logger"
	"API-REST/services/monitor"
	"API-REST/services/storage"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/gofiber/fiber/v2"
)

func (c *Controller) GetAll(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	// Query parameters
	var queryParams payloads.QueryParams
	err := ctx.QueryParser(&queryParams)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(queryParams)
	if err != nil {
		return err
	}

	files, err := c.Model.GetAll(&file.QueryParams{
		Page:            queryParams.Page,
		PageSize:        queryParams.PageSize,
		Any:             queryParams.Any,
		FromDate:        queryParams.FromDate,
		ToDate:          queryParams.ToDate,
		Name:            queryParams.Name,
		ContentType:     queryParams.ContentType,
		Extension:       queryParams.Extension,
		SizeGreaterThan: queryParams.SizeGreaterThan,
		SizeLessThan:    queryParams.SizeLessThan,
		Storage:         queryParams.Storage,
	})
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := make([]*payloads.GetResponse, 0)
	for _, file := range files {
		response = append(response, &payloads.GetResponse{
			ID:          file.ID,
			Name:        file.Name,
			Path:        file.Path,
			ContentType: file.ContentType,
			Extension:   file.Extension,
			Size:        file.Size,
			Storage:     file.Storage,
		})
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Get(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := strconv.Atoi(ctx.Params("id"))
	if err != nil {
		logger.Logger.Print(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	// Get file from DB
	file, err := c.Model.Get(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := payloads.GetResponse{
		ID:          file.ID,
		Name:        file.Name,
		Path:        file.Path,
		ContentType: file.ContentType,
		Extension:   file.Extension,
		Size:        file.Size,
		Storage:     file.Storage,
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Download(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	var err error
	id, ok := ctx.Locals("file_id").(int)
	if !ok {
		id, err = strconv.Atoi(ctx.Params("id"))
		if err != nil {
			logger.Logger.Print(errors.New("invalid id parameter"))
			return util.ErrorJSON(ctx, err)
		}
	}

	// Get file from DB
	f, err := c.Model.Get(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	// Download from storage
	var store storage.Storage
	if f.Storage == storage.Local.Name() {
		store = storage.Local
	} else if f.Storage == storage.GCS.Name() {
		store = storage.GCS
	} else {
		return util.ErrorJSON(ctx, fmt.Errorf("storage %s doesn't exist", f.Storage), http.StatusInternalServerError)
	}
	fileBytes, err := store.GetFile(f.Name, strings.Split(f.Path, "/")...)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	// Serve file
	return util.SendFile(ctx, fileBytes, f.ContentType)
}
func (c *Controller) GetExtensions(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	extensions, err := c.Model.GetExtensions()
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return util.WriteJSON(ctx, http.StatusOK, extensions)
}
func (c *Controller) Insert(hasNextHandler bool, storage storage.Storage, folderPath string, allowedExtensions ...string) func(ctx *fiber.Ctx) error {
	return func(ctx *fiber.Ctx) error {
		// Register metric prometheus
		defer monitor.AddRequestCall(ctx)

		form, err := ctx.MultipartForm()
		if err != nil {
			return util.ErrorJSON(ctx, err)
		}

		var req payloads.InsertRequest
		req.File = form.File["file"][0]

		// Open file
		f, err := req.File.Open()
		if err != nil {
			return util.ErrorJSON(ctx, err)
		}
		defer f.Close()

		// Check allowed extension
		extension := filepath.Ext(req.File.Filename)
		allowed := true
		if len(allowedExtensions) > 0 {
			allowed = false
			for _, ext := range allowedExtensions {
				if ext == extension {
					allowed = true
					break
				}
			}
		}
		if !allowed {
			return util.ErrorJSON(ctx, fmt.Errorf("%s is not a supported file extension", extension))
		}

		// Read fileheader
		content, err := ioutil.ReadAll(f)
		if err != nil {
			return util.ErrorJSON(ctx, err)
		}

		// Get some properties from file
		originalFileName := req.File.Filename
		filePath := folderPath
		contentType := http.DetectContentType(content)
		fileSize := req.File.Size

		// Save file info in db
		fileID, err := c.Model.Insert(&file.File{
			Name:        originalFileName,
			Path:        filePath,
			ContentType: contentType,
			Extension:   extension,
			Size:        int(fileSize),
			Storage:     storage.Name(),
		})
		if err != nil {
			return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
		}
		ctx.Locals("file_id", fmt.Sprint(fileID)) // save file id for possible use in next handlers (relations)

		// Save file in storage
		err = storage.SaveFile(content, originalFileName, strings.Split(filePath, "/")...)
		if err != nil {
			if strings.Contains(fmt.Sprint(err), "file size") {
				return util.ErrorJSON(ctx, err, http.StatusRequestEntityTooLarge)
			}
			return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
		}

		// Set file uploaded in db
		err = c.Model.SetUploaded(fileID)
		if err != nil {
			return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
		}

		if hasNextHandler {
			return ctx.Next()
		} else {
			response := payloads.InsertResponse{ID: fileID}
			return util.WriteJSON(ctx, http.StatusOK, response)
		}
	}
}
func (c *Controller) Delete(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	var err error
	id, ok := ctx.Locals("file_id").(int)
	if !ok {
		id, err = strconv.Atoi(ctx.Params("id"))
		if err != nil {
			logger.Logger.Print(errors.New("invalid id parameter"))
			return util.ErrorJSON(ctx, err)
		}
	}

	// Get file info from DB
	f, err := c.Model.Get(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	// Delete file from storage
	var store storage.Storage
	if f.Storage == storage.Local.Name() {
		store = storage.Local
	} else if f.Storage == storage.GCS.Name() {
		store = storage.GCS
	} else {
		return util.ErrorJSON(ctx, fmt.Errorf("storage %s doesn't exist", f.Storage), http.StatusInternalServerError)
	}
	err = store.DeleteFile(f.Name, strings.Split(f.Path, "/")...)
	if err != nil && !strings.Contains(fmt.Sprint(err), "not found") {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	// Delete file from DB
	err = c.Model.Delete(id)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	return ctx.SendStatus(http.StatusOK)
}
