package file

import (
	"API-REST/services/database/postgres/models/file"
	"API-REST/services/storage"
	"fmt"
	"strings"

	"github.com/go-playground/validator/v10"
)

type Controller struct {
	Validate *validator.Validate
	Model    *file.Model
}

func (c *Controller) PurgeOrphaned() error {
	files, err := c.Model.GetOrphaned()
	if err != nil {
		return err
	}

	for _, f := range files {
		var err error

		// Remove from storage
		if f.Storage == storage.Local.Name() {
			err = storage.Local.DeleteFile(f.Name, strings.Split(f.Path, "/")...)
		} else if f.Storage == storage.GCS.Name() {
			err = storage.GCS.DeleteFile(f.Name, strings.Split(f.Path, "/")...)
		}

		if err != nil && !strings.Contains(fmt.Sprint(err), "not found") {
			return err
		}

		// Remove from db
		err = c.Model.Delete(f.ID)
		if err != nil {
			return err
		}
	}

	return nil
}
