package payloads

import (
	"mime/multipart"
	"time"
)

type QueryParams struct {
	Page            *int       `query:"page"`
	PageSize        *int       `query:"pageSize"`
	Any             *string    `query:"any"`
	Name            *string    `query:"name"`
	ContentType     *string    `query:"contentType"`
	Extension       *string    `query:"extension"`
	SizeGreaterThan *int       `query:"sizeGreaterThan"`
	SizeLessThan    *int       `query:"sizeLessThan"`
	Storage         *string    `query:"storage"`
	FromDate        *time.Time `query:"fromDate" validate:"required_with=ToDate"`
	ToDate          *time.Time `query:"toDate" validate:"required_with=FromDate"`
}

type InsertRequest struct {
	File *multipart.FileHeader `form:"file" validate:"required"`
}
