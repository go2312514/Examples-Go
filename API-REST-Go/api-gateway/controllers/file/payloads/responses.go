package payloads

type GetResponse struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Path        string `json:"path"`
	ContentType string `json:"content_type"`
	Extension   string `json:"extension"`
	Size        int    `json:"size"` // bytes
	Storage     string `json:"storage"`
}

type InsertResponse struct {
	ID int `json:"id"`
}
