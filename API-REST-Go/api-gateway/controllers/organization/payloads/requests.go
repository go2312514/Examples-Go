package payloads

type InsertRequest struct {
	Name string `json:"name" validate:"required"`
	Type string `json:"type" validate:"required"`
}
type UpdateRequest struct {
	Name string `json:"name" validate:"required"`
	Type string `json:"type" validate:"required"`
}

type QueryParams struct {
	Name string `query:"name"`
	Type string `query:"type"`
}
