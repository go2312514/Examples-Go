package payloads

type GetResponse struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Type string `json:"type"`
}

type InsertResponse struct {
	ID string `json:"id"`
}
