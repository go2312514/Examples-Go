package organization

import (
	"API-REST/api-gateway/controllers/organization/payloads"
	util "API-REST/api-gateway/utilities"
	"API-REST/services/database/firestore/models/organization"
	"fmt"
	"net/http"
	"strings"

	"github.com/gofiber/fiber/v2"
)

func (c *Controller) GetAll(ctx *fiber.Ctx) error {
	// Query parameters
	var queryParams payloads.QueryParams
	err := ctx.QueryParser(&queryParams)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(queryParams)
	if err != nil {
		return err
	}

	params := make(map[string]interface{})
	if queryParams.Name != "" {
		params["name"] = queryParams.Name
	}
	if queryParams.Type != "" {
		params["type"] = queryParams.Type
	}

	organizations, err := c.Model.GetAll(ctx.Context(), params)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := make([]*payloads.GetResponse, 0)
	for _, organization := range organizations {
		response = append(response, &payloads.GetResponse{
			ID:   organization.ID,
			Name: organization.Name,
			Type: organization.Type.String(),
		})
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Get(ctx *fiber.Ctx) error {
	id := ctx.Params("id")

	organization, err := c.Model.Get(ctx.Context(), id)
	if err != nil {
		errMsg := fmt.Sprint(err)
		if strings.Contains(errMsg, "missing") || strings.Contains(errMsg, "not found") {
			return util.ErrorJSON(ctx, err)
		}
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	response := payloads.GetResponse{
		ID:   organization.ID,
		Name: organization.Name,
		Type: organization.Type.String(),
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Insert(ctx *fiber.Ctx) error {
	var req payloads.InsertRequest

	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	orgType, err := organization.GetOrganizationType(req.Type)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	id, err := c.Model.Insert(ctx.Context(), &organization.Organization{
		Name: req.Name,
		Type: orgType,
	})
	if err != nil {
		errMsg := fmt.Sprint(err)
		if strings.Contains(errMsg, "missing") || strings.Contains(errMsg, "exists") {
			return util.ErrorJSON(ctx, err)
		}
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	response := payloads.InsertResponse{ID: id}
	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Update(ctx *fiber.Ctx) error {
	id := ctx.Params("id")

	var req payloads.UpdateRequest

	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	orgType, err := organization.GetOrganizationType(req.Type)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	id, err = c.Model.Update(ctx.Context(), &organization.Organization{
		ID:   id,
		Name: req.Name,
		Type: orgType,
	})
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := payloads.InsertResponse{ID: id}
	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Delete(ctx *fiber.Ctx) error {
	id := ctx.Params("id")

	err := c.Model.Delete(ctx.Context(), id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
