package organization

import (
	"API-REST/services/database/firestore/models/organization"

	"github.com/go-playground/validator/v10"
)

type Controller struct {
	Validate *validator.Validate
	Model    *organization.Model
}
