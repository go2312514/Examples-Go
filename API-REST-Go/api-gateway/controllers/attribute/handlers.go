package attribute

import (
	"API-REST/api-gateway/controllers/attribute/payloads"
	util "API-REST/api-gateway/utilities"
	"API-REST/services/database/mongo/models/attribute"
	"net/http"
	"time"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (c *Controller) GetAll(ctx *fiber.Ctx) error {

	// Query parameters
	filterOptions := make(map[string]interface{})
	fromDate, toDate, err := c.getDateRange(ctx.Query("fromDate"), ctx.Query("toDate"))
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	nameParam := ctx.Query("name")
	labelParam := ctx.Query("label")
	if len(nameParam) != 0 {
		filterOptions["name"] = nameParam
	}
	if len(labelParam) != 0 {
		filterOptions["label"] = labelParam
	}
	// other filter options...

	attributes, err := c.Model.GetAll(fromDate, toDate, filterOptions)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := make([]*payloads.GetResponse, 0)
	for _, attribute := range attributes {
		response = append(response, &payloads.GetResponse{
			ID:        attribute.ID,
			Metadata:  attribute.Metadata,
			Timestamp: attribute.Timestamp,
			Value:     *attribute.Value,
		})
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Get(ctx *fiber.Ctx) error {
	id := ctx.Params("id")

	attribute, err := c.Model.Get(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := payloads.GetResponse{
		ID:        attribute.ID,
		Metadata:  attribute.Metadata,
		Timestamp: attribute.Timestamp,
		Value:     *attribute.Value,
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Insert(ctx *fiber.Ctx) error {
	var req payloads.InsertRequest

	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	var attributes []*attribute.Attribute
	for _, a := range req.Attributes {
		timestamp, err := time.Parse("2006-01-02T15:04:05", a.Timestamp)
		if err != nil {
			return util.ErrorJSON(ctx, err)
		}
		attributes = append(attributes,
			&attribute.Attribute{
				Metadata: attribute.AttributeMetadata{
					AssetName: a.AssetName,
					Name:      a.Name,
					Label:     a.Label,
					Unit:      a.Unit,
				},
				Timestamp: &timestamp,
				Value:     &a.Value,
			},
		)
	}

	if len(attributes) == 1 {
		id, err := c.Model.Insert(attributes[0])
		if err != nil {
			return util.ErrorJSON(ctx, err)
		}
		response := payloads.InsertResponse{ID: id}
		return util.WriteJSON(ctx, http.StatusOK, response)
	} else {
		ids, err := c.Model.InsertMany(attributes)
		if err != nil {
			return util.ErrorJSON(ctx, err)
		}
		return util.WriteJSON(ctx, http.StatusOK, ids)
	}
}
func (c *Controller) Update(ctx *fiber.Ctx) error {
	id := ctx.Params("id")

	var req payloads.UpdateRequest
	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	var timestamp time.Time
	if req.Timestamp != nil {
		timestamp, err = time.Parse("2006-01-02T15:04:05", *req.Timestamp)
		if err != nil {
			return util.ErrorJSON(ctx, err)
		}
	}

	var a attribute.Attribute
	a.ID, err = primitive.ObjectIDFromHex(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	a.Metadata = attribute.AttributeMetadata{}
	if req.AssetName != nil {
		a.Metadata.AssetName = *req.AssetName
	}
	if req.Name != nil {
		a.Metadata.Name = *req.Name
	}
	if req.Label != nil {
		a.Metadata.Label = *req.Label
	}
	if req.Unit != nil {
		a.Metadata.Unit = *req.Unit
	}
	a.Timestamp = &timestamp
	a.Value = req.Value

	err = c.Model.Update(&a)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) Delete(ctx *fiber.Ctx) error {
	id := ctx.Params("id")

	err := c.Model.Delete(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
