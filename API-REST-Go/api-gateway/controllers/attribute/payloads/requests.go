package payloads

type InsertRequest struct {
	Attributes []struct {
		AssetName string  `json:"asset_name" validate:"required"`
		Name      string  `json:"name" validate:"required"`
		Label     string  `json:"label" validate:"required"`
		Unit      string  `json:"unit" validate:"required"`
		Timestamp string  `json:"timestamp" validate:"required"`
		Value     float64 `json:"value" validate:"required"`
	} `json:"attributes" validate:"required,dive"`
}
type UpdateRequest struct {
	AssetName *string  `json:"asset_name"`
	Name      *string  `json:"name"`
	Label     *string  `json:"label"`
	Unit      *string  `json:"unit"`
	Timestamp *string  `json:"timestamp"`
	Value     *float64 `json:"value"`
}
