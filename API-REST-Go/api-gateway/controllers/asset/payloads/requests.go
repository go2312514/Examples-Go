package payloads

type InsertRequest struct {
	Assets []struct {
		Name string `json:"name" validate:"required"`
		Date string `json:"date" validate:"required"`
	} `json:"assets" validate:"required,dive"`
}
type UpdateRequest struct {
	Name *string `json:"name"`
	Date *string `json:"date"`
}
