package payloads

import (
	attrPayloads "API-REST/api-gateway/controllers/attribute/payloads"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type GetResponse struct {
	ID         primitive.ObjectID          `json:"id" bson:"_id, omitempty"`
	Name       string                      `json:"name,omitempty" bson:"name"`
	Date       *time.Time                  `json:"date,omitempty" bson:"date"`
	CreatedAt  *time.Time                  `json:"created_at,omitempty" bson:"created_at"`
	UpdatedAt  *time.Time                  `json:"updated_at,omitempty" bson:"updated_at"`
	Attributes []*attrPayloads.GetResponse `json:"attributes,omitempty" bson:"attributes"`
}

type InsertResponse struct {
	ID primitive.ObjectID `json:"id"`
}
