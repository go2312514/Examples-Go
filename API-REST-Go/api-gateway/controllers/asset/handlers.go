package asset

import (
	"API-REST/api-gateway/controllers/asset/payloads"
	attrPayloads "API-REST/api-gateway/controllers/attribute/payloads"
	util "API-REST/api-gateway/utilities"
	"API-REST/services/database/mongo/models/asset"
	"net/http"
	"time"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (c *Controller) GetAll(ctx *fiber.Ctx) error {

	// Query parameters
	filterOptions := make(map[string]interface{})
	fromDate, toDate, err := c.getDateRange(ctx.Query("fromDate"), ctx.Query("toDate"))
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	nameParam := ctx.Query("name")
	if len(nameParam) != 0 {
		filterOptions["name"] = nameParam
	}
	// other filter options...

	assets, err := c.Model.GetAll(fromDate, toDate, filterOptions)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := make([]*payloads.GetResponse, 0)
	for _, asset := range assets {
		response = append(response, &payloads.GetResponse{
			ID:        asset.ID,
			Name:      asset.Name,
			Date:      asset.Date,
			CreatedAt: asset.CreatedAt,
			UpdatedAt: asset.UpdatedAt,
		})
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Get(ctx *fiber.Ctx) error {
	id := ctx.Params("id")

	asset, err := c.Model.Get(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := payloads.GetResponse{
		ID:         asset.ID,
		Name:       asset.Name,
		Date:       asset.Date,
		CreatedAt:  asset.CreatedAt,
		UpdatedAt:  asset.UpdatedAt,
		Attributes: []*attrPayloads.GetResponse{},
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) GetWithAttributes(ctx *fiber.Ctx) error {
	id := ctx.Params("id")

	asset, err := c.Model.GetWithAttributes(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := payloads.GetResponse{
		ID:         asset.ID,
		Name:       asset.Name,
		Date:       asset.Date,
		CreatedAt:  asset.CreatedAt,
		UpdatedAt:  asset.UpdatedAt,
		Attributes: []*attrPayloads.GetResponse{},
	}
	for _, attr := range asset.Attributes {
		response.Attributes = append(response.Attributes, &attrPayloads.GetResponse{
			ID:        attr.ID,
			Metadata:  attr.Metadata,
			Timestamp: attr.Timestamp,
			Value:     *attr.Value,
		})
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) GetNames(ctx *fiber.Ctx) error {
	fromDate, toDate, err := c.getDateRange(ctx.Query("fromDate"), ctx.Query("toDate"))
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	assets, err := c.Model.GetNames(fromDate, toDate)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	var response []*payloads.GetResponse
	for _, asset := range assets {
		response = append(response, &payloads.GetResponse{
			Name: asset.Name,
		})
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Insert(ctx *fiber.Ctx) error {
	var req payloads.InsertRequest
	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	var assets []*asset.Asset
	for _, a := range req.Assets {
		date, err := time.Parse("2006-01-02", a.Date)
		if err != nil {
			return util.ErrorJSON(ctx, err)
		}
		assets = append(assets, &asset.Asset{Name: a.Name, Date: &date})
	}

	if len(assets) == 1 {
		id, err := c.Model.Insert(assets[0])
		if err != nil {
			return util.ErrorJSON(ctx, err)
		}
		response := payloads.InsertResponse{ID: id}
		return util.WriteJSON(ctx, http.StatusOK, response)
	} else {
		ids, err := c.Model.InsertMany(assets)
		if err != nil {
			return util.ErrorJSON(ctx, err)
		}
		return util.WriteJSON(ctx, http.StatusOK, ids)
	}
}
func (c *Controller) Update(ctx *fiber.Ctx) error {
	id := ctx.Params("id")

	var req payloads.UpdateRequest
	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	var date time.Time
	if req.Date != nil {
		date, err = time.Parse("2006-01-02", *req.Date)
		if err != nil {
			return util.ErrorJSON(ctx, err)
		}
	}

	var a asset.Asset
	a.ID, err = primitive.ObjectIDFromHex(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	if req.Name != nil {
		a.Name = *req.Name
	}
	a.Date = &date

	err = c.Model.Update(&a)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) Delete(ctx *fiber.Ctx) error {
	id := ctx.Params("id")

	err := c.Model.Delete(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
