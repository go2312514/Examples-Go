package controllers

import (
	"API-REST/api-gateway/controllers/asset"
	"API-REST/api-gateway/controllers/attribute"
	"API-REST/api-gateway/controllers/feature"
	"API-REST/api-gateway/controllers/file"
	"API-REST/api-gateway/controllers/log"
	"API-REST/api-gateway/controllers/organization"
	"API-REST/api-gateway/controllers/permission"
	"API-REST/api-gateway/controllers/role"
	"API-REST/api-gateway/controllers/user"
	"API-REST/api-gateway/controllers/user/auth"
	firestore "API-REST/services/database/firestore/models"
	mongo "API-REST/services/database/mongo/models"
	psql "API-REST/services/database/postgres/models"

	"github.com/go-playground/validator/v10"
)

var (
	User         *user.Controller
	Role         *role.Controller
	Permission   *permission.Controller
	File         *file.Controller
	Log          *log.Controller
	Feature      *feature.Controller
	Asset        *asset.Controller
	Attribute    *attribute.Controller
	Organization *organization.Controller
	// ...
)

func Build() {
	validate := validator.New()

	User = &user.Controller{Validate: validate, Model: psql.User, Auth: &auth.Controller{Validate: validate, Model: psql.User}}
	Role = &role.Controller{Validate: validate, Model: psql.Role}
	Permission = &permission.Controller{Validate: validate, Model: psql.Permission}
	File = &file.Controller{Validate: validate, Model: psql.File}
	Log = &log.Controller{Validate: validate}
	Feature = &feature.Controller{Validate: validate, Model: psql.Feature}
	Asset = &asset.Controller{Validate: validate, Model: mongo.Asset}
	Attribute = &attribute.Controller{Validate: validate, Model: mongo.Attribute}
	Organization = &organization.Controller{Validate: validate, Model: firestore.Organization}
	// ...
}

func PurgeOrphanedData() error {
	err := File.PurgeOrphaned()
	if err != nil {
		return err
	}

	return nil
}
