package log

import (
	util "API-REST/api-gateway/utilities"
	"API-REST/services/logger"
	"API-REST/services/monitor"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

func (c *Controller) GetAll(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	logs, err := logger.GetAll()
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	response := logs

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Get(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	filename := ctx.Params("filename")

	contentLines, err := logger.Get(filename)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusBadRequest)
	}

	response := contentLines

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Delete(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	filename := ctx.Params("filename")

	err := logger.Delete(filename)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusBadRequest)
	}

	return ctx.SendStatus(http.StatusOK)
}
