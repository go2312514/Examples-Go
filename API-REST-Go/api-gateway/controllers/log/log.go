package log

import (
	"github.com/go-playground/validator/v10"
)

type Controller struct {
	Validate *validator.Validate
}
