package feature

import (
	"API-REST/api-gateway/controllers/feature/payloads"
	util "API-REST/api-gateway/utilities"
	"API-REST/services/database/postgres/models/feature"
	"API-REST/services/gis"
	"API-REST/services/monitor"
	"fmt"
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/proxy"
	"github.com/google/uuid"
)

func (c *Controller) GetAll(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	var features []*feature.Feature
	var err error
	// Query parameters
	var queryParams payloads.QueryParams
	err = ctx.QueryParser(&queryParams)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(queryParams)
	if err != nil {
		return err
	}

	features, err = c.Model.GetAll(&feature.QueryParams{
		Page:       queryParams.Page,
		PageSize:   queryParams.PageSize,
		FromDate:   queryParams.FromDate,
		ToDate:     queryParams.ToDate,
		UserID:     queryParams.UserID,
		MostRecent: queryParams.MostRecent,
	})
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := make([]*payloads.GetResponse, 0)
	for _, feature := range features {
		response = append(response, &payloads.GetResponse{
			Geom:      feature.Geom,
			Timestamp: feature.Timestamp,
			UserID:    feature.UserID,
		})
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Insert(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))

	var req payloads.InsertRequest
	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	err = c.Model.Insert(&feature.Feature{Geom: req.Geom, Timestamp: req.Timestamp, UserID: claimerID})
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}

func (c *Controller) GetVectorTiles(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	proxyPath := ctx.Params("*")
	queryParams := ctx.Context().QueryArgs().QueryString()
	if len(queryParams) > 0 {
		proxyPath += "?" + string(queryParams)
	}

	url := fmt.Sprintf("%s/%s",
		gis.Url,
		proxyPath)
	// Proxy
	err := proxy.Do(ctx, url)
	if err != nil {
		return err
	}
	// Remove Server header from response
	ctx.Response().Header.Del(fiber.HeaderServer)
	return nil
}
