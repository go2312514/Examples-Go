package payloads

import (
	"time"

	"github.com/cridenour/go-postgis"
	"github.com/google/uuid"
)

type QueryParams struct {
	Page       *int       `query:"page" validate:"required_with=PageSize"`
	PageSize   *int       `query:"pageSize" validate:"required_with=Page"`
	FromDate   *time.Time `query:"fromDate" validate:"required_with=ToDate"`
	ToDate     *time.Time `query:"toDate" validate:"required_with=FromDate"`
	UserID     *uuid.UUID `query:"user_id"`
	MostRecent *bool      `query:"most_recent"`
}

type InsertRequest struct {
	Geom      *postgis.PointS `json:"geom" validate:"required"`
	Timestamp time.Time       `json:"timestamp" validate:"required"`
}
