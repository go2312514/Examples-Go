package user

import (
	"API-REST/api-gateway/controllers/user/payloads"
	util "API-REST/api-gateway/utilities"
	"API-REST/services/conf"
	"API-REST/services/database/postgres/models/user"
	"API-REST/services/logger"
	"API-REST/services/mail"
	"API-REST/services/marshaller"
	"API-REST/services/monitor"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"strconv"

	"github.com/gocarina/gocsv"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

func (c *Controller) GetAll(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	// Query parameters
	var queryParams payloads.QueryParams
	err := ctx.QueryParser(&queryParams)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(queryParams)
	if err != nil {
		return err
	}

	usrs, err := c.Model.GetAll(&user.QueryParams{
		Page:     queryParams.Page,
		PageSize: queryParams.PageSize,
		Any:      queryParams.Any,
		Deleted:  queryParams.Deleted,
		Banned:   queryParams.Banned,
		Year:     queryParams.Year,
	})
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := make([]*payloads.GetResponse, 0)
	for _, user := range usrs {
		response = append(response, &payloads.GetResponse{
			ID:        user.ID,
			Username:  user.Username,
			Email:     user.Email,
			FirstName: user.FirstName,
			LastName:  user.LastName,
		})
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) Get(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := uuid.Parse(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	u, err := c.Model.Get(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := payloads.GetResponse{
		ID:        u.ID,
		Username:  u.Username,
		Email:     u.Email,
		FirstName: u.FirstName,
		LastName:  u.LastName,
	}
	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) DownloadPhoto(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := uuid.Parse(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	// Get photo ID from db
	photoID, err := c.Model.GetPhotoID(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	// Save photo ID in context for next usage
	ctx.Locals("file_id", photoID)

	return ctx.Next()
}
func (c *Controller) DownloadCV(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := uuid.Parse(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	// Get CV ID from db
	cvID, err := c.Model.GetCVID(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	// Save CV ID in context for next usage
	ctx.Locals("file_id", cvID)

	return ctx.Next()
}
func (c *Controller) Insert(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	var req payloads.InsertRequest

	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	hashedPassword, err := c.HashPassword(req.Password)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	var u user.User
	u.Username = req.Username
	u.Email = req.Email
	u.Password = hashedPassword
	if req.Nick != nil {
		u.Nick = *req.Nick
	}
	u.FirstName = req.FirstName
	u.LastName = req.LastName
	u.Phone = req.Phone
	u.Address = req.Address

	err = c.Model.Insert(&u)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	id, _ := c.Model.GetIDByEmail(u.Email)
	token, _ := c.Auth.GenerateJwtToken(id, conf.Env.GetString("JWT_CONFIRM_EMAIL_SECRET"))
	mail.Send(&mail.Mail{
		From:    conf.Env.GetString("MAIL_FROM_NOREPLY"),
		To:      []string{u.Email},
		Subject: "Confirm email",
		Body:    c.Auth.GenerateConfirmationEmail(token),
	})

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) Update(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := uuid.Parse(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	var req payloads.UpdateRequest
	err = ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	var u user.User
	u.ID = id
	if req.Nick != nil {
		u.Nick = *req.Nick
	}
	u.FirstName = req.FirstName
	u.LastName = req.LastName
	u.Phone = req.Phone
	u.Address = req.Address

	err = c.Model.Update(&u)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) UpdateRoles(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := uuid.Parse(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	var req payloads.UpdateRolesRequest

	err = ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	err = c.Model.UpdateRoles(id, req.RoleIDs...)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) UpdatePhoto(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := uuid.Parse(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	photoIDStr, ok := ctx.Locals("file_id").(string)
	if !ok {
		return util.ErrorJSON(ctx, errors.New("error parsing file_id to string"), http.StatusInternalServerError)
	}
	photoID, err := strconv.Atoi(photoIDStr)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	err = c.Model.UpdatePhoto(id, photoID)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) UpdateCV(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := uuid.Parse(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	cvIDStr, ok := ctx.Locals("file_id").(string)
	if !ok {
		return util.ErrorJSON(ctx, errors.New("error parsing file_id to string"), http.StatusInternalServerError)
	}
	cvID, err := strconv.Atoi(cvIDStr)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	err = c.Model.UpdateCV(id, cvID)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) DeletePhoto(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := uuid.Parse(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	photoID, err := c.Model.GetPhotoID(id)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}
	err = c.Model.DeletePhoto(id)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	// Save CV ID in context for next usage
	ctx.Locals("file_id", photoID)

	return ctx.Next()
}
func (c *Controller) DeleteCV(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := uuid.Parse(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	cvID, err := c.Model.GetCVID(id)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}
	err = c.Model.DeleteCV(id)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	// Save CV ID in context for next usage
	ctx.Locals("file_id", cvID)

	return ctx.Next()
}
func (c *Controller) Ban(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := uuid.Parse(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	var req payloads.BanRequest
	err = ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	err = c.Model.Ban(id, req.BanExpire)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) Unban(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := uuid.Parse(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	err = c.Model.Unban(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) Restore(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := uuid.Parse(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	err = c.Model.Restore(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) Delete(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	id, err := uuid.Parse(ctx.Params("id"))
	if err != nil {
		logger.Logger.Println(errors.New("invalid id parameter"))
		return util.ErrorJSON(ctx, err)
	}

	err = c.Model.Delete(id)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) ImportCSV(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	form, err := ctx.MultipartForm()
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	var req payloads.ImportCSVRequest
	req.File = form.File["file"][0]

	// Open file
	f, err := req.File.Open()
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	defer f.Close()

	// Check allowed extension
	extension := filepath.Ext(req.File.Filename)
	if extension != ".csv" {
		return util.ErrorJSON(ctx, fmt.Errorf("%s is not a supported file extension", extension))
	}

	// Read fileheader
	content, err := ioutil.ReadAll(f)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	// CSV to []user
	usrs, err := marshaller.CSVToUsers(content)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	// Insert all users
	err = c.Model.InsertMany(usrs)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) ExportCSV(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	// Query parameters
	var queryParams payloads.QueryParams
	err := ctx.QueryParser(&queryParams)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	// Get all users
	usrs, err := c.Model.GetAll(&user.QueryParams{
		Page:     queryParams.Page,
		PageSize: queryParams.PageSize,
		Any:      queryParams.Any,
		Deleted:  queryParams.Deleted,
		Banned:   queryParams.Banned,
		Year:     queryParams.Year,
	})
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	// Send response as CSV
	ctx.Response().Header.Set("Content-Type", "text/csv")
	ctx.Response().Header.Set("Content-Disposition", "attachment;filename=users.csv")
	err = gocsv.Marshal(usrs, ctx.Response().BodyWriter())
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)

	}
	return nil
}
