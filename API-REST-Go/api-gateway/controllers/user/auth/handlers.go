package auth

import (
	"API-REST/api-gateway/controllers/user/auth/payloads"
	util "API-REST/api-gateway/utilities"
	"API-REST/services/conf"
	"API-REST/services/database/postgres/models/user"
	"API-REST/services/mail"
	"API-REST/services/monitor"
	"API-REST/services/sms"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

const randomResetPasswordToken = "FlFPeLcZmVOhHipvWU8C4HMWyLKs1mPdqFeW" // 36 length

func (c *Controller) Login(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	var req payloads.LoginRequest
	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	var u *user.User
	if req.Email != "" {
		u, err = c.Model.GetByEmailWithPassword(req.Email)
		if err != nil {
			return util.ErrorJSON(ctx, err, http.StatusUnauthorized)
		}
	} else {
		u, err = c.Model.GetByUsernameWithPassword(req.Username)
		if err != nil {
			return util.ErrorJSON(ctx, err, http.StatusUnauthorized)
		}
	}

	if u.DeletedAt != nil {
		return util.ErrorJSON(ctx, errors.New("user deleted"), http.StatusUnauthorized)
	}
	if u.BanDate != nil {
		if u.BanExpire.Before(time.Now()) {
			c.Model.Unban(u.ID)
		} else {
			return util.WriteJSON(ctx, http.StatusUnauthorized, payloads.LoginResponse{BanExpire: u.BanExpire}, "error")
		}
	}

	hashedPassword := u.Password

	err = c.compareHashAndPassword(hashedPassword, req.Password)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusUnauthorized)
	}

	// Generate jwt token after successful login
	token, err := c.GenerateJwtToken(u.ID, conf.Env.GetString("JWT_LOGIN_SECRET"))
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusNotImplemented)
	}

	return util.WriteJSON(ctx, http.StatusOK, payloads.LoginResponse{Token: string(token)})
}
func (c *Controller) ConfirmEmail(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	token := ctx.Params("token")

	// Validate Token
	userID, err := c.ValidateJwtToken([]byte(token), conf.Env.GetString("JWT_CONFIRM_EMAIL_SECRET"))
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusUnauthorized)
	}

	// Check email is not already verified
	verified, err := c.Model.HasVerifiedEmail(userID)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusConflict)
	}
	if verified {
		return util.ErrorJSON(ctx, errors.New("email already verified"))
	}

	// Verify email
	err = c.Model.VerifyEmail(userID)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) SendConfirmationEmail(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))

	u, err := c.Model.Get(claimerID)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	if u.VerifiedEmail {
		return util.ErrorJSON(ctx, errors.New("email already verified"))
	}

	token, err := c.GenerateJwtToken(claimerID, conf.Env.GetString("JWT_CONFIRM_EMAIL_SECRET"))
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	err = mail.Send(&mail.Mail{
		From:    conf.Env.GetString("MAIL_FROM_NOREPLY"),
		To:      []string{u.Email},
		Subject: "Confirm email",
		Body:    c.GenerateConfirmationEmail(token),
	})
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) ConfirmPhone(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	code := ctx.Params("code")
	if len(code) < 6 {
		return util.ErrorJSON(ctx, errors.New("invalid code"))
	}
	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))
	// Check phone is not already verified
	verified, err := c.Model.HasVerifiedPhone(claimerID)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusConflict)
	}
	if verified {
		return util.ErrorJSON(ctx, errors.New("phone already verified"))
	}

	// Init params required to compute code
	claimerToken := strings.Split(ctx.Get("Authorization"), " ")[1]
	secret := conf.Env.GetString("CONFIRM_PHONE_SECRET")
	codeDuration := conf.Env.GetString("CONFIRM_PHONE_DURATION")
	duration, err := strconv.Atoi(codeDuration)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	// Compute codes from last <duration> minutes and compare with given code
	valid, err := c.validate6DigitsCode(code, duration, claimerID, claimerToken, secret)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}
	if !valid {
		return util.ErrorJSON(ctx, errors.New("invalid code"))
	}
	err = c.Model.VerifyPhone(claimerID)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}
	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) SendConfirmationPhone(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))
	claimerToken := strings.Split(ctx.Get("Authorization"), " ")[1]

	u, err := c.Model.Get(claimerID)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	if u.Phone == nil {
		return util.ErrorJSON(ctx, errors.New("user hasn't phone"))
	}
	if u.VerifiedPhone {
		return util.ErrorJSON(ctx, errors.New("phone already verified"))
	}

	code, err := c.compute6DigitsCode(
		claimerID,
		claimerToken,
		conf.Env.GetString("CONFIRM_PHONE_SECRET"),
		time.Now(),
	)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	err = sms.Send(*u.Phone, code)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) ForgotPassword(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	var req payloads.ForgotPasswordRequest

	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	u, err := c.Model.GetByEmail(req.Email)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	if u.DeletedAt != nil {
		return util.ErrorJSON(ctx, errors.New("user deleted"), http.StatusUnauthorized)
	}
	if u.BanDate != nil {
		return util.ErrorJSON(ctx, errors.New("user banned"), http.StatusUnauthorized)
	}

	code, err := c.compute6DigitsCode(
		u.ID,
		randomResetPasswordToken,
		conf.Env.GetString("RESET_PASSWORD_SECRET"),
		time.Now(),
	)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	err = mail.Send(&mail.Mail{
		From:    conf.Env.GetString("MAIL_FROM_NOREPLY"),
		To:      []string{u.Email},
		Subject: "Reset password",
		Body:    code,
	})
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) ResetPassword(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	var req payloads.ResetPasswordRequest

	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	u, err := c.Model.GetByEmail(req.Email)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	if u.DeletedAt != nil {
		return util.ErrorJSON(ctx, errors.New("user deleted"), http.StatusUnauthorized)
	}
	if u.BanDate != nil {
		return util.ErrorJSON(ctx, errors.New("user banned"), http.StatusUnauthorized)
	}

	id, err := c.Model.GetIDByEmail(req.Email)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	secret := conf.Env.GetString("RESET_PASSWORD_SECRET")
	codeDuration := conf.Env.GetString("RESET_PASSWORD_DURATION")
	duration, err := strconv.Atoi(codeDuration)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}
	// Validate code
	valid, err := c.validate6DigitsCode(
		req.Code,
		duration,
		id,
		randomResetPasswordToken,
		secret,
	)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}
	if !valid {
		return util.ErrorJSON(ctx, errors.New("invalid code"))
	}
	// Code is valid -> Reset password
	hashedPassword, err := c.hashPassword(req.NewPassword)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Model.UpdatePassword(id, hashedPassword)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	return ctx.SendStatus(http.StatusOK)
}

func (c *Controller) Get(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))

	u, err := c.Model.Get(claimerID)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	response := payloads.GetResponse{
		CreatedAt:          u.CreatedAt,
		Username:           u.Username,
		Email:              u.Email,
		Nick:               u.Nick,
		FirstName:          *u.FirstName,
		LastName:           *u.LastName,
		Phone:              *u.Phone,
		Address:            *u.Address,
		LastPasswordChange: u.LastPasswordChange,
		VerifiedEmail:      u.VerifiedEmail,
		VerifiedPhone:      u.VerifiedPhone,
	}

	return util.WriteJSON(ctx, http.StatusOK, response)
}
func (c *Controller) DownloadPhoto(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))

	// Get photo ID from db
	photoID, err := c.Model.GetPhotoID(claimerID)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	// Save photo ID in context for next usage
	ctx.Locals("file_id", photoID)

	return ctx.Next()
}
func (c *Controller) DownloadCV(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))

	// Get CV ID from db
	cvID, err := c.Model.GetCVID(claimerID)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	// Save CV ID in context for next usage
	ctx.Locals("file_id", cvID)

	return ctx.Next()
}
func (c *Controller) Update(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))

	var req payloads.UpdateRequest
	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	var u user.User
	u.ID = claimerID
	u.Nick = req.Nick
	u.FirstName = &req.FirstName
	u.LastName = &req.LastName
	u.Phone = &req.Phone
	u.Address = &req.Address

	err = c.Model.Update(&u)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) ChangePassword(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))

	var req payloads.ChangePasswordRequest

	err := ctx.BodyParser(&req)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}
	err = c.Validate.Struct(req)
	if err != nil {
		return err
	}

	hashedPassword, err := c.Model.GetPassword(claimerID)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	err = c.compareHashAndPassword(hashedPassword, req.OldPassword)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusUnauthorized)
	}

	hashedNewPassword, err := c.hashPassword(req.NewPassword)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	err = c.Model.UpdatePassword(claimerID, hashedNewPassword)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) UpdatePhoto(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))

	photoIDStr, ok := ctx.Locals("file_id").(string)
	if !ok {
		return util.ErrorJSON(ctx, errors.New("error parsing file_id to string"), http.StatusInternalServerError)
	}
	photoID, err := strconv.Atoi(photoIDStr)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	err = c.Model.UpdatePhoto(claimerID, photoID)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) UpdateCV(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))

	cvIDStr, ok := ctx.Locals("file_id").(string)
	if !ok {
		return util.ErrorJSON(ctx, errors.New("error parsing file_id to string"), http.StatusInternalServerError)
	}
	cvID, err := strconv.Atoi(cvIDStr)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	err = c.Model.UpdateCV(claimerID, cvID)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	return ctx.SendStatus(http.StatusOK)
}
func (c *Controller) DeletePhoto(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))

	photoID, err := c.Model.GetPhotoID(claimerID)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}
	err = c.Model.DeletePhoto(claimerID)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	// Save CV ID in context for next usage
	ctx.Locals("file_id", photoID)

	return ctx.Next()
}
func (c *Controller) DeleteCV(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))

	cvID, err := c.Model.GetCVID(claimerID)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}
	err = c.Model.DeleteCV(claimerID)
	if err != nil {
		return util.ErrorJSON(ctx, err, http.StatusInternalServerError)
	}

	// Save CV ID in context for next usage
	ctx.Locals("file_id", cvID)

	return ctx.Next()
}
func (c *Controller) Delete(ctx *fiber.Ctx) error {
	// Register metric prometheus
	defer monitor.AddRequestCall(ctx)

	claimerID := uuid.MustParse(ctx.Locals("Claimer-ID").(string))

	err := c.Model.Delete(claimerID)
	if err != nil {
		return util.ErrorJSON(ctx, err)
	}

	return ctx.SendStatus(http.StatusOK)
}
