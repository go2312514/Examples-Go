package payloads

import "time"

type GetResponse struct {
	CreatedAt          time.Time  `json:"created_at"`
	Username           string     `json:"username"`
	Email              string     `json:"email"`
	Nick               string     `json:"nick"`
	FirstName          string     `json:"first_name,omitempty"`
	LastName           string     `json:"last_name,omitempty"`
	Phone              string     `json:"phone,omitempty"`
	Address            string     `json:"address,omitempty"`
	LastPasswordChange *time.Time `json:"last_password_change,omitempty"`
	VerifiedEmail      bool       `json:"verified_email,omitempty"`
	VerifiedPhone      bool       `json:"verified_phone,omitempty"`
}

type LoginResponse struct {
	Token     string     `json:"token,omitempty"`
	BanExpire *time.Time `json:"ban_expire,omitempty"`
}
