package payloads

import (
	"mime/multipart"
	"time"
)

type QueryParams struct {
	Page     *int    `query:"page" validate:"required_with=PageSize"`
	PageSize *int    `query:"pageSize" validate:"required_with=Page"`
	Any      *string `query:"any"`
	Deleted  *bool   `query:"deleted"`
	Banned   *bool   `query:"banned"`
	Year     *int    `query:"year"`
}

type InsertRequest struct {
	Username  string  `json:"username" validate:"required"`
	Email     string  `json:"email" validate:"required,email,min=6,max=32"`
	Password  string  `json:"password" validate:"required"`
	Nick      *string `json:"nick,omitempty"`
	FirstName *string `json:"first_name,omitempty"`
	LastName  *string `json:"last_name,omitempty"`
	Phone     *string `json:"phone,omitempty"`
	Address   *string `json:"address,omitempty"`
}

type UpdateRequest struct {
	Nick      *string `json:"nick,omitempty"`
	FirstName *string `json:"first_name,omitempty"`
	LastName  *string `json:"last_name,omitempty"`
	Phone     *string `json:"phone,omitempty"`
	Address   *string `json:"address,omitempty"`
}

type UpdateRolesRequest struct {
	RoleIDs []int `json:"role_ids" validate:"required"`
}

type BanRequest struct {
	BanExpire time.Time `json:"ban_expire" validate:"required" example:"2006-01-02T00:00:00Z"`
}

type ImportCSVRequest struct {
	File *multipart.FileHeader `form:"file" validate:"required"`
}
